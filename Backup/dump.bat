@echo off
SET PGUSER=postgres
SET PGPASSWORD=123456
SET PGHOME=C:\Program Files\PostgreSQL\9.4
C:
chdir C:\trabalho\Backup
del locadora*.sql
echo "Aguarde, realizando o backup do Banco de Dados"
for /f "tokens=1,2,3 delims=/ " %%a in ('DATE /T') do set DATE=%%c-%%b-%%a
"%PGHOME%\bin\pg_dump" -i -b -v -C -o -f "C:\trabalho\Backup\locadora%DATE%.sql" db_locadora
Exit