--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.5
-- Dumped by pg_dump version 9.4.5
-- Started on 2015-12-07 16:08:51

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 2282 (class 1262 OID 18035)
-- Name: db_locadora; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE db_locadora WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Portuguese_Brazil.1252' LC_CTYPE = 'Portuguese_Brazil.1252';


ALTER DATABASE db_locadora OWNER TO postgres;

\connect db_locadora

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 212 (class 3079 OID 11855)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2285 (class 0 OID 0)
-- Dependencies: 212
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 213 (class 1255 OID 18036)
-- Name: funct_insert_data(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funct_insert_data() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
Begin
  new.DT_CADASTRO = CURRENT_TIMESTAMP;
  new.DT_MANUTENCAO = CURRENT_TIMESTAMP;
  return new;
end;

$$;


ALTER FUNCTION public.funct_insert_data() OWNER TO postgres;

--
-- TOC entry 226 (class 1255 OID 18037)
-- Name: funct_update_data(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funct_update_data() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
Begin
  new.DT_MANUTENCAO = CURRENT_TIMESTAMP;
  return new;
end;

$$;


ALTER FUNCTION public.funct_update_data() OWNER TO postgres;

--
-- TOC entry 227 (class 1255 OID 18038)
-- Name: validarcnpj(character varying[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION validarcnpj(cnpj character varying[]) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $_$
DECLARE
 v_string text := $1;
 v_caldv1 int4;
 v_caldv2 int4;
 v_dv1 int4;
 v_dv2 int4;
 v_array1 text[] ;
 v_array2 text[] ;
 v_tst_string int4;
 BEGIN
 v_string := translate(v_string, './-', '');
 IF (char_length(v_string)::int4) = 14 THEN

  SELECT INTO v_array1 '{5,4,3,2,9,8,7,6,5,4,3,2}';
  SELECT INTO v_array2 '{6,5,4,3,2,9,8,7,6,5,4,3,2}';
  v_dv1 := (substring(v_string, 13, 1))::int4;
  v_dv2 := (substring(v_string, 14, 1))::int4;
  /* COLETA DIG VER 1 CNPJ */
  v_caldv1 := 0;
  FOR va IN 1..12 LOOP
   v_caldv1 := v_caldv1 + ((SELECT substring(v_string, va, 1))::int4 * (v_array1[va]::int4));
  END LOOP;
  v_caldv1 := v_caldv1 % 11;
   IF (v_caldv1 = 0) OR (v_caldv1 = 1) THEN
    v_caldv1 := 0;
   ELSE
    v_caldv1 := 11 - v_caldv1;
   END IF;
  /* COLETA DIG VER 2 CNPJ */
  v_caldv2 := 0;
  FOR va IN 1..13 LOOP
   v_caldv2 := v_caldv2 + ((SELECT substring(v_string || v_caldv1::text, va, 1))::int4 * (v_array2[va]::int4));
  END LOOP;
  v_caldv2 := v_caldv2 % 11;
   IF (v_caldv2 = 0) OR (v_caldv2 = 1) THEN
    v_caldv2 := 0;
   ELSE
    v_caldv2 := 11 - v_caldv2;
   END IF;
  /* TESTA */
  IF (v_caldv1 = v_dv1) AND (v_caldv2 = v_dv2) THEN
   RETURN TRUE;
  ELSE
   RETURN FALSE;
  END IF;
end if;
end;
$_$;


ALTER FUNCTION public.validarcnpj(cnpj character varying[]) OWNER TO postgres;

--
-- TOC entry 228 (class 1255 OID 18039)
-- Name: validarcpf(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION validarcpf(par_cpf character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
-- ROTINA DE VALIDAÇÃO DE CPF
-- Conversão para o PL/ PGSQL: Cláudio Leopoldino - http://postgresqlbr.blogspot.com/
-- Algoritmo original: http://webmasters.neting.com/msg07743.html
-- Retorna 1 para CPF correto.
DECLARE
x real;
y real; --Variável temporária
soma integer;
dig1 integer; --Primeiro dígito do CPF
dig2 integer; --Segundo dígito do CPF
len integer; -- Tamanho do CPF
contloop integer; --Contador para loop
val_par_cpf varchar(11); --Valor do parâmetro
BEGIN
-- Teste do tamanho da string de entrada
IF char_length(par_cpf) = 11 THEN
ELSE
RAISE NOTICE 'Formato inválido: %',$1;
RETURN 0;
END IF;
-- Inicialização
x := 0;
soma := 0;
dig1 := 0;
dig2 := 0;
contloop := 0;
val_par_cpf := $1; --Atribuição do parâmetro a uma variável interna
len := char_length(val_par_cpf);
x := len -1;
--Loop de multiplicação - dígito 1
contloop :=1;
WHILE contloop <= (len -2) LOOP
y := CAST(substring(val_par_cpf from contloop for 1) AS NUMERIC);
soma := soma + ( y * x);
x := x - 1;
contloop := contloop +1;
END LOOP;
dig1 := 11 - CAST((soma % 11) AS INTEGER);
if (dig1 = 10) THEN dig1 :=0 ; END IF;
if (dig1 = 11) THEN dig1 :=0 ; END IF;

-- Dígito 2
x := 11; soma :=0;
contloop :=1;
WHILE contloop <= (len -1) LOOP
soma := soma + CAST((substring(val_par_cpf FROM contloop FOR 1)) AS REAL) * x;
x := x - 1;
contloop := contloop +1;
END LOOP;
dig2 := 11 - CAST ((soma % 11) AS INTEGER);
IF (dig2 = 10) THEN dig2 := 0; END IF;
IF (dig2 = 11) THEN dig2 := 0; END IF;
--Teste do CPF
IF ((dig1 || '' || dig2) = substring(val_par_cpf FROM len-1 FOR 2)) THEN
RETURN 1;
ELSE
RAISE NOTICE 'DV do CPF Inválido: %',$1;
RETURN 0;
END IF;
END;
$_$;


ALTER FUNCTION public.validarcpf(par_cpf character varying) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 172 (class 1259 OID 18040)
-- Name: tb_aluguel; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_aluguel (
    cd_codigo integer NOT NULL,
    cd_filme integer NOT NULL,
    dt_data_inicio date NOT NULL,
    dt_data_final date NOT NULL,
    dt_data_efetiva date,
    vl_custo_aluguel numeric(8,2),
    cd_pedido integer,
    cd_pessoa integer,
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_aluguel OWNER TO postgres;

--
-- TOC entry 2286 (class 0 OID 0)
-- Dependencies: 172
-- Name: TABLE tb_aluguel; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_aluguel IS 'Tabela de alugueis';


--
-- TOC entry 2287 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN tb_aluguel.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_aluguel.cd_codigo IS 'Código do aluguel';


--
-- TOC entry 2288 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN tb_aluguel.cd_filme; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_aluguel.cd_filme IS 'Código do filme do aluguel';


--
-- TOC entry 2289 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN tb_aluguel.dt_data_inicio; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_aluguel.dt_data_inicio IS 'Data inicial do aluguel';


--
-- TOC entry 2290 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN tb_aluguel.dt_data_final; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_aluguel.dt_data_final IS 'Data final do aluguel';


--
-- TOC entry 2291 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN tb_aluguel.dt_data_efetiva; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_aluguel.dt_data_efetiva IS 'Data efetiva do aluguel';


--
-- TOC entry 2292 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN tb_aluguel.vl_custo_aluguel; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_aluguel.vl_custo_aluguel IS 'Valor do aluguel';


--
-- TOC entry 2293 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN tb_aluguel.cd_pedido; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_aluguel.cd_pedido IS 'Código do pedido';


--
-- TOC entry 2294 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN tb_aluguel.cd_pessoa; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_aluguel.cd_pessoa IS 'Código da pessoa';


--
-- TOC entry 2295 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN tb_aluguel.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_aluguel.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2296 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN tb_aluguel.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_aluguel.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2297 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN tb_aluguel.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_aluguel.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 173 (class 1259 OID 18043)
-- Name: tb_aluguel_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_aluguel_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_aluguel_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2298 (class 0 OID 0)
-- Dependencies: 173
-- Name: tb_aluguel_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_aluguel_cd_codigo_seq OWNED BY tb_aluguel.cd_codigo;


--
-- TOC entry 174 (class 1259 OID 18045)
-- Name: tb_bairro; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_bairro (
    cd_codigo integer NOT NULL,
    tx_descricao character varying(30) NOT NULL,
    cd_municipio integer NOT NULL,
    nr_cep integer,
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_bairro OWNER TO postgres;

--
-- TOC entry 2299 (class 0 OID 0)
-- Dependencies: 174
-- Name: TABLE tb_bairro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_bairro IS 'Tabela de bairros';


--
-- TOC entry 2300 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN tb_bairro.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_bairro.cd_codigo IS 'Código do bairro';


--
-- TOC entry 2301 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN tb_bairro.tx_descricao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_bairro.tx_descricao IS 'Descrição do bairro';


--
-- TOC entry 2302 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN tb_bairro.nr_cep; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_bairro.nr_cep IS 'CEP do bairro';


--
-- TOC entry 2303 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN tb_bairro.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_bairro.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2304 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN tb_bairro.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_bairro.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2305 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN tb_bairro.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_bairro.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 175 (class 1259 OID 18048)
-- Name: tb_bairro_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_bairro_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_bairro_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2306 (class 0 OID 0)
-- Dependencies: 175
-- Name: tb_bairro_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_bairro_cd_codigo_seq OWNED BY tb_bairro.cd_codigo;


--
-- TOC entry 176 (class 1259 OID 18050)
-- Name: tb_compra; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_compra (
    cd_codigo integer NOT NULL,
    cd_fornecedor integer,
    vl_valor integer,
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_compra OWNER TO postgres;

--
-- TOC entry 2307 (class 0 OID 0)
-- Dependencies: 176
-- Name: TABLE tb_compra; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_compra IS 'Tabela de compras';


--
-- TOC entry 2308 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN tb_compra.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_compra.cd_codigo IS 'Código da comprado';


--
-- TOC entry 2309 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN tb_compra.cd_fornecedor; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_compra.cd_fornecedor IS 'Código do fornecedor';


--
-- TOC entry 2310 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN tb_compra.vl_valor; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_compra.vl_valor IS 'Valor da compra';


--
-- TOC entry 2311 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN tb_compra.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_compra.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2312 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN tb_compra.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_compra.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2313 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN tb_compra.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_compra.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 177 (class 1259 OID 18053)
-- Name: tb_compra_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_compra_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_compra_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2314 (class 0 OID 0)
-- Dependencies: 177
-- Name: tb_compra_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_compra_cd_codigo_seq OWNED BY tb_compra.cd_codigo;


--
-- TOC entry 178 (class 1259 OID 18055)
-- Name: tb_compraitem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_compraitem (
    cd_codigo integer NOT NULL,
    cd_midia integer,
    vl_preco integer,
    nr_quantidade integer,
    cd_compra integer,
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_compraitem OWNER TO postgres;

--
-- TOC entry 2315 (class 0 OID 0)
-- Dependencies: 178
-- Name: TABLE tb_compraitem; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_compraitem IS 'Tabela de compras';


--
-- TOC entry 2316 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_compraitem.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_compraitem.cd_codigo IS 'Código do item comprado';


--
-- TOC entry 2317 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_compraitem.cd_midia; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_compraitem.cd_midia IS 'Tipo da mídia';


--
-- TOC entry 2318 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_compraitem.vl_preco; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_compraitem.vl_preco IS 'Valor do item';


--
-- TOC entry 2319 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_compraitem.nr_quantidade; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_compraitem.nr_quantidade IS 'Quantidade comprada';


--
-- TOC entry 2320 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_compraitem.cd_compra; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_compraitem.cd_compra IS 'Código da compra';


--
-- TOC entry 2321 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_compraitem.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_compraitem.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2322 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_compraitem.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_compraitem.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2323 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_compraitem.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_compraitem.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 179 (class 1259 OID 18058)
-- Name: tb_compraitem_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_compraitem_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_compraitem_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2324 (class 0 OID 0)
-- Dependencies: 179
-- Name: tb_compraitem_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_compraitem_cd_codigo_seq OWNED BY tb_compraitem.cd_codigo;


--
-- TOC entry 180 (class 1259 OID 18060)
-- Name: tb_elenco; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_elenco (
    cd_codigo integer NOT NULL,
    cd_filme integer NOT NULL,
    tx_nome character varying(40) NOT NULL,
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_elenco OWNER TO postgres;

--
-- TOC entry 2325 (class 0 OID 0)
-- Dependencies: 180
-- Name: TABLE tb_elenco; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_elenco IS 'Tabela de elencos';


--
-- TOC entry 2326 (class 0 OID 0)
-- Dependencies: 180
-- Name: COLUMN tb_elenco.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_elenco.cd_codigo IS 'Código do elenco';


--
-- TOC entry 2327 (class 0 OID 0)
-- Dependencies: 180
-- Name: COLUMN tb_elenco.cd_filme; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_elenco.cd_filme IS 'Código do filme do elenco';


--
-- TOC entry 2328 (class 0 OID 0)
-- Dependencies: 180
-- Name: COLUMN tb_elenco.tx_nome; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_elenco.tx_nome IS 'Nomes do elenco';


--
-- TOC entry 2329 (class 0 OID 0)
-- Dependencies: 180
-- Name: COLUMN tb_elenco.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_elenco.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2330 (class 0 OID 0)
-- Dependencies: 180
-- Name: COLUMN tb_elenco.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_elenco.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2331 (class 0 OID 0)
-- Dependencies: 180
-- Name: COLUMN tb_elenco.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_elenco.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 181 (class 1259 OID 18063)
-- Name: tb_elenco_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_elenco_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_elenco_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2332 (class 0 OID 0)
-- Dependencies: 181
-- Name: tb_elenco_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_elenco_cd_codigo_seq OWNED BY tb_elenco.cd_codigo;


--
-- TOC entry 182 (class 1259 OID 18065)
-- Name: tb_estado; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_estado (
    cd_codigo integer NOT NULL,
    tx_descricao character varying(30) NOT NULL,
    tx_sigla character varying(2) NOT NULL,
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_estado OWNER TO postgres;

--
-- TOC entry 2333 (class 0 OID 0)
-- Dependencies: 182
-- Name: TABLE tb_estado; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_estado IS 'Tabela de estados';


--
-- TOC entry 2334 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN tb_estado.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_estado.cd_codigo IS 'Código do estado';


--
-- TOC entry 2335 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN tb_estado.tx_descricao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_estado.tx_descricao IS 'Descrição do estado';


--
-- TOC entry 2336 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN tb_estado.tx_sigla; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_estado.tx_sigla IS 'Sigla do estado';


--
-- TOC entry 2337 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN tb_estado.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_estado.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2338 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN tb_estado.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_estado.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2339 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN tb_estado.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_estado.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 183 (class 1259 OID 18068)
-- Name: tb_estado_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_estado_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_estado_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2340 (class 0 OID 0)
-- Dependencies: 183
-- Name: tb_estado_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_estado_cd_codigo_seq OWNED BY tb_estado.cd_codigo;


--
-- TOC entry 184 (class 1259 OID 18070)
-- Name: tb_estoque; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_estoque (
    cd_codigo integer NOT NULL,
    cd_filme integer NOT NULL,
    nr_quantidade integer,
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_estoque OWNER TO postgres;

--
-- TOC entry 2341 (class 0 OID 0)
-- Dependencies: 184
-- Name: TABLE tb_estoque; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_estoque IS 'Tabela de estoques';


--
-- TOC entry 2342 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN tb_estoque.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_estoque.cd_codigo IS 'Código do estoque';


--
-- TOC entry 2343 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN tb_estoque.cd_filme; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_estoque.cd_filme IS 'Código do filme no estoque';


--
-- TOC entry 2344 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN tb_estoque.nr_quantidade; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_estoque.nr_quantidade IS 'Quantidade do estoque';


--
-- TOC entry 2345 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN tb_estoque.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_estoque.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2346 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN tb_estoque.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_estoque.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2347 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN tb_estoque.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_estoque.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 185 (class 1259 OID 18073)
-- Name: tb_estoque_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_estoque_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_estoque_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2348 (class 0 OID 0)
-- Dependencies: 185
-- Name: tb_estoque_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_estoque_cd_codigo_seq OWNED BY tb_estoque.cd_codigo;


--
-- TOC entry 186 (class 1259 OID 18075)
-- Name: tb_filme; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_filme (
    cd_codigo integer NOT NULL,
    cd_midia integer NOT NULL,
    tx_nome character varying(40) NOT NULL,
    cd_genero integer,
    nr_classificacao integer,
    vl_preco_diaria numeric(8,2),
    dt_lancamento date,
    nr_duracao integer,
    tx_idioma character varying(40),
    tx_legenda character varying(40),
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_filme OWNER TO postgres;

--
-- TOC entry 2349 (class 0 OID 0)
-- Dependencies: 186
-- Name: TABLE tb_filme; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_filme IS 'Tabela de filmes';


--
-- TOC entry 2350 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN tb_filme.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_filme.cd_codigo IS 'Código do filme';


--
-- TOC entry 2351 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN tb_filme.cd_midia; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_filme.cd_midia IS 'Código da mídia do filme';


--
-- TOC entry 2352 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN tb_filme.tx_nome; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_filme.tx_nome IS 'Nome do fime';


--
-- TOC entry 2353 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN tb_filme.cd_genero; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_filme.cd_genero IS 'Genero do filme';


--
-- TOC entry 2354 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN tb_filme.nr_classificacao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_filme.nr_classificacao IS 'Classificação do filme';


--
-- TOC entry 2355 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN tb_filme.vl_preco_diaria; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_filme.vl_preco_diaria IS 'Valor da diária do filme';


--
-- TOC entry 2356 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN tb_filme.dt_lancamento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_filme.dt_lancamento IS 'Data de lançamento do filme';


--
-- TOC entry 2357 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN tb_filme.nr_duracao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_filme.nr_duracao IS 'Duração do filmeo';


--
-- TOC entry 2358 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN tb_filme.tx_idioma; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_filme.tx_idioma IS 'Idioma do filme';


--
-- TOC entry 2359 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN tb_filme.tx_legenda; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_filme.tx_legenda IS 'Legenda do filme';


--
-- TOC entry 2360 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN tb_filme.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_filme.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2361 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN tb_filme.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_filme.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2362 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN tb_filme.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_filme.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 187 (class 1259 OID 18078)
-- Name: tb_filme_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_filme_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_filme_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2363 (class 0 OID 0)
-- Dependencies: 187
-- Name: tb_filme_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_filme_cd_codigo_seq OWNED BY tb_filme.cd_codigo;


--
-- TOC entry 188 (class 1259 OID 18080)
-- Name: tb_fornecedor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_fornecedor (
    cd_codigo integer NOT NULL,
    tx_cnpj character varying(14),
    tx_endereco character varying(45),
    cd_municipio integer,
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_fornecedor OWNER TO postgres;

--
-- TOC entry 2364 (class 0 OID 0)
-- Dependencies: 188
-- Name: TABLE tb_fornecedor; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_fornecedor IS 'Tabela de fornecedores';


--
-- TOC entry 2365 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN tb_fornecedor.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_fornecedor.cd_codigo IS 'Código do fornecedor';


--
-- TOC entry 2366 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN tb_fornecedor.tx_cnpj; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_fornecedor.tx_cnpj IS 'CNPJ do fornecedor';


--
-- TOC entry 2367 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN tb_fornecedor.tx_endereco; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_fornecedor.tx_endereco IS 'Endereço do fornecedor';


--
-- TOC entry 2368 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN tb_fornecedor.cd_municipio; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_fornecedor.cd_municipio IS 'Código do municipio do fornecedor';


--
-- TOC entry 2369 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN tb_fornecedor.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_fornecedor.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2370 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN tb_fornecedor.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_fornecedor.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2371 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN tb_fornecedor.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_fornecedor.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 189 (class 1259 OID 18083)
-- Name: tb_fornecedor_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_fornecedor_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_fornecedor_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2372 (class 0 OID 0)
-- Dependencies: 189
-- Name: tb_fornecedor_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_fornecedor_cd_codigo_seq OWNED BY tb_fornecedor.cd_codigo;


--
-- TOC entry 190 (class 1259 OID 18085)
-- Name: tb_genero; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_genero (
    cd_codigo integer NOT NULL,
    tx_descricao character varying(40),
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_genero OWNER TO postgres;

--
-- TOC entry 2373 (class 0 OID 0)
-- Dependencies: 190
-- Name: TABLE tb_genero; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_genero IS 'Tabela de generos';


--
-- TOC entry 2374 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN tb_genero.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_genero.cd_codigo IS 'Código do genero';


--
-- TOC entry 2375 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN tb_genero.tx_descricao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_genero.tx_descricao IS 'Descrição do genero';


--
-- TOC entry 2376 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN tb_genero.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_genero.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2377 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN tb_genero.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_genero.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2378 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN tb_genero.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_genero.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 191 (class 1259 OID 18088)
-- Name: tb_genero_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_genero_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_genero_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2379 (class 0 OID 0)
-- Dependencies: 191
-- Name: tb_genero_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_genero_cd_codigo_seq OWNED BY tb_genero.cd_codigo;


--
-- TOC entry 192 (class 1259 OID 18090)
-- Name: tb_imagem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_imagem (
    cd_codigo integer NOT NULL,
    cd_filme integer NOT NULL,
    tx_descricao character varying(40) NOT NULL,
    bt_foto bytea,
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_imagem OWNER TO postgres;

--
-- TOC entry 2380 (class 0 OID 0)
-- Dependencies: 192
-- Name: TABLE tb_imagem; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_imagem IS 'Tabela de imagens';


--
-- TOC entry 2381 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN tb_imagem.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_imagem.cd_codigo IS 'Código da imagem';


--
-- TOC entry 2382 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN tb_imagem.cd_filme; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_imagem.cd_filme IS 'Código do filme da imagem';


--
-- TOC entry 2383 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN tb_imagem.tx_descricao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_imagem.tx_descricao IS 'Descrição da imagem';


--
-- TOC entry 2384 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN tb_imagem.bt_foto; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_imagem.bt_foto IS 'Imagem';


--
-- TOC entry 2385 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN tb_imagem.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_imagem.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2386 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN tb_imagem.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_imagem.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2387 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN tb_imagem.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_imagem.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 193 (class 1259 OID 18096)
-- Name: tb_imagem_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_imagem_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_imagem_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2388 (class 0 OID 0)
-- Dependencies: 193
-- Name: tb_imagem_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_imagem_cd_codigo_seq OWNED BY tb_imagem.cd_codigo;


--
-- TOC entry 194 (class 1259 OID 18098)
-- Name: tb_midia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_midia (
    cd_codigo integer NOT NULL,
    tx_tipomidia character varying(10),
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_midia OWNER TO postgres;

--
-- TOC entry 2389 (class 0 OID 0)
-- Dependencies: 194
-- Name: TABLE tb_midia; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_midia IS 'Tabela de mídias';


--
-- TOC entry 2390 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN tb_midia.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_midia.cd_codigo IS 'Código da mídia';


--
-- TOC entry 2391 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN tb_midia.tx_tipomidia; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_midia.tx_tipomidia IS 'Tipo da mídia';


--
-- TOC entry 2392 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN tb_midia.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_midia.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2393 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN tb_midia.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_midia.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2394 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN tb_midia.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_midia.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 195 (class 1259 OID 18101)
-- Name: tb_midia_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_midia_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_midia_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2395 (class 0 OID 0)
-- Dependencies: 195
-- Name: tb_midia_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_midia_cd_codigo_seq OWNED BY tb_midia.cd_codigo;


--
-- TOC entry 196 (class 1259 OID 18103)
-- Name: tb_municipio; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_municipio (
    cd_codigo integer NOT NULL,
    tx_descricao character varying(40) NOT NULL,
    cd_estado integer,
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_municipio OWNER TO postgres;

--
-- TOC entry 2396 (class 0 OID 0)
-- Dependencies: 196
-- Name: TABLE tb_municipio; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_municipio IS 'Tabela de municipios';


--
-- TOC entry 2397 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN tb_municipio.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_municipio.cd_codigo IS 'Código do municipio';


--
-- TOC entry 2398 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN tb_municipio.tx_descricao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_municipio.tx_descricao IS 'Descrição do municipio';


--
-- TOC entry 2399 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN tb_municipio.cd_estado; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_municipio.cd_estado IS 'Código do estado do municipio';


--
-- TOC entry 2400 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN tb_municipio.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_municipio.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2401 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN tb_municipio.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_municipio.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2402 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN tb_municipio.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_municipio.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 197 (class 1259 OID 18106)
-- Name: tb_municipio_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_municipio_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_municipio_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2403 (class 0 OID 0)
-- Dependencies: 197
-- Name: tb_municipio_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_municipio_cd_codigo_seq OWNED BY tb_municipio.cd_codigo;


--
-- TOC entry 198 (class 1259 OID 18108)
-- Name: tb_pedido; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_pedido (
    cd_codigo integer NOT NULL,
    cd_filme integer,
    dt_data_pedido date,
    dt_data_disponibilidade date,
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_pedido OWNER TO postgres;

--
-- TOC entry 2404 (class 0 OID 0)
-- Dependencies: 198
-- Name: TABLE tb_pedido; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_pedido IS 'Tabela de pedidos';


--
-- TOC entry 2405 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN tb_pedido.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_pedido.cd_codigo IS 'Código do pedido';


--
-- TOC entry 2406 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN tb_pedido.cd_filme; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_pedido.cd_filme IS 'Código do filme do pedido';


--
-- TOC entry 2407 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN tb_pedido.dt_data_pedido; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_pedido.dt_data_pedido IS 'Data da disponibilidade dos filmes do pedido';


--
-- TOC entry 2408 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN tb_pedido.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_pedido.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2409 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN tb_pedido.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_pedido.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2410 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN tb_pedido.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_pedido.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 199 (class 1259 OID 18111)
-- Name: tb_pedido_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_pedido_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_pedido_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2411 (class 0 OID 0)
-- Dependencies: 199
-- Name: tb_pedido_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_pedido_cd_codigo_seq OWNED BY tb_pedido.cd_codigo;


--
-- TOC entry 200 (class 1259 OID 18113)
-- Name: tb_pessoa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_pessoa (
    cd_codigo integer NOT NULL,
    tx_nome character varying(40) NOT NULL,
    tx_cpf character varying(11) NOT NULL,
    tx_sexo character(1),
    tx_endereco character varying(40),
    cd_municipio integer,
    bt_foto bytea,
    cd_tipo integer,
    dt_nascimento date,
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_pessoa OWNER TO postgres;

--
-- TOC entry 2412 (class 0 OID 0)
-- Dependencies: 200
-- Name: TABLE tb_pessoa; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_pessoa IS 'Tabela de pessoas';


--
-- TOC entry 2413 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN tb_pessoa.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_pessoa.cd_codigo IS 'Código da pessoa';


--
-- TOC entry 2414 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN tb_pessoa.tx_cpf; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_pessoa.tx_cpf IS 'CPF da pessoa';


--
-- TOC entry 2415 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN tb_pessoa.tx_endereco; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_pessoa.tx_endereco IS 'Endereço da pessoa';


--
-- TOC entry 2416 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN tb_pessoa.cd_municipio; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_pessoa.cd_municipio IS 'Municipio da pessoa';


--
-- TOC entry 2417 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN tb_pessoa.bt_foto; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_pessoa.bt_foto IS 'Foto da pessoa';


--
-- TOC entry 2418 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN tb_pessoa.cd_tipo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_pessoa.cd_tipo IS 'Tipo da pessoa';


--
-- TOC entry 2419 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN tb_pessoa.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_pessoa.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2420 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN tb_pessoa.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_pessoa.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2421 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN tb_pessoa.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_pessoa.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 201 (class 1259 OID 18119)
-- Name: tb_pessoa_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_pessoa_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_pessoa_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2422 (class 0 OID 0)
-- Dependencies: 201
-- Name: tb_pessoa_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_pessoa_cd_codigo_seq OWNED BY tb_pessoa.cd_codigo;


--
-- TOC entry 202 (class 1259 OID 18121)
-- Name: tb_tipo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_tipo (
    cd_codigo integer NOT NULL,
    tx_descricao character varying(20),
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_tipo OWNER TO postgres;

--
-- TOC entry 2423 (class 0 OID 0)
-- Dependencies: 202
-- Name: TABLE tb_tipo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_tipo IS 'Tabela de tipos';


--
-- TOC entry 2424 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN tb_tipo.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_tipo.cd_codigo IS 'Código do tipo';


--
-- TOC entry 2425 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN tb_tipo.tx_descricao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_tipo.tx_descricao IS 'Descrição do tipo';


--
-- TOC entry 2426 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN tb_tipo.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_tipo.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2427 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN tb_tipo.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_tipo.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2428 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN tb_tipo.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_tipo.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 203 (class 1259 OID 18124)
-- Name: tb_tipo_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_tipo_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_tipo_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2429 (class 0 OID 0)
-- Dependencies: 203
-- Name: tb_tipo_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_tipo_cd_codigo_seq OWNED BY tb_tipo.cd_codigo;


--
-- TOC entry 204 (class 1259 OID 18126)
-- Name: tb_trailer; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_trailer (
    cd_codigo integer NOT NULL,
    cd_filme integer,
    tx_descricao character varying(40),
    bt_trailer bytea,
    dt_cadastro date,
    dt_manutencao date,
    dt_exclusao date
);


ALTER TABLE tb_trailer OWNER TO postgres;

--
-- TOC entry 2430 (class 0 OID 0)
-- Dependencies: 204
-- Name: TABLE tb_trailer; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tb_trailer IS 'Tabela de trailers';


--
-- TOC entry 2431 (class 0 OID 0)
-- Dependencies: 204
-- Name: COLUMN tb_trailer.cd_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_trailer.cd_codigo IS 'Código do trailer';


--
-- TOC entry 2432 (class 0 OID 0)
-- Dependencies: 204
-- Name: COLUMN tb_trailer.cd_filme; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_trailer.cd_filme IS 'Código do filme do trailer';


--
-- TOC entry 2433 (class 0 OID 0)
-- Dependencies: 204
-- Name: COLUMN tb_trailer.tx_descricao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_trailer.tx_descricao IS 'Descrição do trailer';


--
-- TOC entry 2434 (class 0 OID 0)
-- Dependencies: 204
-- Name: COLUMN tb_trailer.bt_trailer; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_trailer.bt_trailer IS 'Video do trailer ';


--
-- TOC entry 2435 (class 0 OID 0)
-- Dependencies: 204
-- Name: COLUMN tb_trailer.dt_cadastro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_trailer.dt_cadastro IS 'Data de cadastro';


--
-- TOC entry 2436 (class 0 OID 0)
-- Dependencies: 204
-- Name: COLUMN tb_trailer.dt_manutencao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_trailer.dt_manutencao IS 'Data de manutenção';


--
-- TOC entry 2437 (class 0 OID 0)
-- Dependencies: 204
-- Name: COLUMN tb_trailer.dt_exclusao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tb_trailer.dt_exclusao IS 'Data de exclusão';


--
-- TOC entry 205 (class 1259 OID 18132)
-- Name: tb_trailer_cd_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_trailer_cd_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_trailer_cd_codigo_seq OWNER TO postgres;

--
-- TOC entry 2438 (class 0 OID 0)
-- Dependencies: 205
-- Name: tb_trailer_cd_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_trailer_cd_codigo_seq OWNED BY tb_trailer.cd_codigo;


--
-- TOC entry 206 (class 1259 OID 18134)
-- Name: vw_rel_alugueis; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vw_rel_alugueis AS
 SELECT a.cd_codigo,
    p.tx_nome AS pessoa,
    f.tx_nome AS filme,
    g.tx_descricao,
    a.dt_data_inicio,
    a.dt_data_efetiva,
    a.vl_custo_aluguel
   FROM ((((tb_aluguel a
     JOIN tb_pessoa p ON ((a.cd_pessoa = p.cd_codigo)))
     JOIN tb_tipo t ON ((p.cd_tipo = t.cd_codigo)))
     JOIN tb_filme f ON ((a.cd_filme = f.cd_codigo)))
     JOIN tb_genero g ON ((f.cd_genero = g.cd_codigo)))
  WHERE ((((((t.tx_descricao)::text = 'Cliente'::text) AND (p.tx_sexo = 'F'::bpchar)) AND (((date_part('month'::text, a.dt_data_inicio))::integer % 2) = 0)) AND (date_part('year'::text, f.dt_lancamento) < (2014)::double precision)) AND ((g.tx_descricao)::text = ANY (ARRAY[('Ação'::character varying)::text, ('Comédia'::character varying)::text, ('Suspense'::character varying)::text])))
  ORDER BY p.tx_nome, a.dt_data_inicio DESC;


ALTER TABLE vw_rel_alugueis OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 18139)
-- Name: vw_rel_aniversariantes; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vw_rel_aniversariantes AS
 SELECT tb_pessoa.cd_codigo,
    tb_pessoa.tx_nome,
    tb_pessoa.tx_cpf,
    tb_pessoa.tx_sexo,
    tb_pessoa.tx_endereco,
    tb_pessoa.cd_municipio,
    tb_pessoa.bt_foto,
    tb_pessoa.cd_tipo,
    tb_pessoa.dt_nascimento,
    tb_pessoa.dt_cadastro,
    tb_pessoa.dt_manutencao,
    tb_pessoa.dt_exclusao
   FROM tb_pessoa
  WHERE (date_part('month'::text, tb_pessoa.dt_nascimento) = date_part('month'::text, now()));


ALTER TABLE vw_rel_aniversariantes OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 18143)
-- Name: vw_rel_clientes; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vw_rel_clientes AS
 SELECT p.tx_nome,
    p.tx_cpf,
    count(a.cd_codigo) AS quantidade,
    sum(a.vl_custo_aluguel) AS valortotal
   FROM ((tb_pessoa p
     JOIN tb_tipo t ON ((p.cd_tipo = t.cd_codigo)))
     JOIN tb_aluguel a ON ((a.cd_pessoa = p.cd_codigo)))
  WHERE (((t.tx_descricao)::text = 'Cliente'::text) AND (date_part('year'::text, a.dt_data_inicio) = ANY (ARRAY[(2014)::double precision, (2015)::double precision])))
  GROUP BY p.tx_nome, p.tx_cpf, date_part('month'::text, a.dt_data_inicio)
 HAVING ((count(a.cd_codigo) > 5) AND (sum(a.vl_custo_aluguel) > (30)::numeric))
  ORDER BY count(a.cd_codigo) DESC;


ALTER TABLE vw_rel_clientes OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 18148)
-- Name: vw_rel_clientes_2; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vw_rel_clientes_2 AS
 SELECT p.tx_nome AS pessoa,
    p.tx_cpf,
    p.tx_sexo,
    p.dt_cadastro,
    f.tx_nome AS filme,
    f.nr_duracao
   FROM (((tb_aluguel a
     JOIN tb_pessoa p ON ((a.cd_pessoa = p.cd_codigo)))
     JOIN tb_tipo t ON ((p.cd_tipo = t.cd_codigo)))
     JOIN tb_filme f ON ((a.cd_filme = f.cd_codigo)))
  WHERE ((((t.tx_descricao)::text = 'Cliente'::text) AND (f.nr_duracao > 120)) AND (((date_part('year'::text, now()))::integer - (date_part('year'::text, p.dt_cadastro))::integer) > 5))
  ORDER BY p.tx_nome;


ALTER TABLE vw_rel_clientes_2 OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 18153)
-- Name: vw_rel_filmes; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vw_rel_filmes AS
 SELECT f.tx_nome AS filme,
    p.tx_nome AS pessoa,
    m.tx_descricao AS municipio,
    f.dt_lancamento AS lancamento,
    f.nr_classificacao
   FROM ((((tb_aluguel a
     JOIN tb_pessoa p ON ((a.cd_pessoa = p.cd_codigo)))
     JOIN tb_filme f ON ((a.cd_filme = f.cd_codigo)))
     JOIN tb_tipo t ON ((p.cd_tipo = t.cd_codigo)))
     JOIN tb_municipio m ON ((p.cd_municipio = m.cd_codigo)))
  WHERE ((((((t.tx_descricao)::text = 'Cliente'::text) AND ((m.tx_descricao)::text = ANY (ARRAY[('Bandeirante'::character varying)::text, ('Descanso'::character varying)::text, ('Paraíso'::character varying)::text]))) AND (f.nr_classificacao >= 14)) AND (date_part('year'::text, f.dt_lancamento) >= (2014)::double precision)) AND (date_part('year'::text, f.dt_lancamento) <= (2015)::double precision))
  ORDER BY f.tx_nome;


ALTER TABLE vw_rel_filmes OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 18158)
-- Name: vw_rel_filmes_alugados; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vw_rel_filmes_alugados AS
 SELECT f.cd_codigo,
    f.tx_nome,
    m.tx_tipomidia,
    a.vl_custo_aluguel,
    a.dt_data_inicio,
    a.dt_data_final
   FROM ((tb_filme f
     JOIN tb_aluguel a ON ((a.cd_filme = f.cd_codigo)))
     JOIN tb_midia m ON ((f.cd_midia = m.cd_codigo)));


ALTER TABLE vw_rel_filmes_alugados OWNER TO postgres;

--
-- TOC entry 2008 (class 2604 OID 18163)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_aluguel ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_aluguel_cd_codigo_seq'::regclass);


--
-- TOC entry 2009 (class 2604 OID 18164)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_bairro ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_bairro_cd_codigo_seq'::regclass);


--
-- TOC entry 2010 (class 2604 OID 18165)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_compra ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_compra_cd_codigo_seq'::regclass);


--
-- TOC entry 2011 (class 2604 OID 18166)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_compraitem ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_compraitem_cd_codigo_seq'::regclass);


--
-- TOC entry 2012 (class 2604 OID 18167)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_elenco ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_elenco_cd_codigo_seq'::regclass);


--
-- TOC entry 2013 (class 2604 OID 18168)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_estado ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_estado_cd_codigo_seq'::regclass);


--
-- TOC entry 2014 (class 2604 OID 18169)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_estoque ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_estoque_cd_codigo_seq'::regclass);


--
-- TOC entry 2015 (class 2604 OID 18170)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_filme ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_filme_cd_codigo_seq'::regclass);


--
-- TOC entry 2016 (class 2604 OID 18171)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_fornecedor ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_fornecedor_cd_codigo_seq'::regclass);


--
-- TOC entry 2017 (class 2604 OID 18172)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_genero ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_genero_cd_codigo_seq'::regclass);


--
-- TOC entry 2018 (class 2604 OID 18173)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_imagem ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_imagem_cd_codigo_seq'::regclass);


--
-- TOC entry 2019 (class 2604 OID 18174)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_midia ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_midia_cd_codigo_seq'::regclass);


--
-- TOC entry 2020 (class 2604 OID 18175)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_municipio ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_municipio_cd_codigo_seq'::regclass);


--
-- TOC entry 2021 (class 2604 OID 18176)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_pedido ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_pedido_cd_codigo_seq'::regclass);


--
-- TOC entry 2022 (class 2604 OID 18177)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_pessoa ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_pessoa_cd_codigo_seq'::regclass);


--
-- TOC entry 2023 (class 2604 OID 18178)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_tipo ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_tipo_cd_codigo_seq'::regclass);


--
-- TOC entry 2024 (class 2604 OID 18179)
-- Name: cd_codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_trailer ALTER COLUMN cd_codigo SET DEFAULT nextval('tb_trailer_cd_codigo_seq'::regclass);


--
-- TOC entry 2244 (class 0 OID 18040)
-- Dependencies: 172
-- Data for Name: tb_aluguel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_aluguel (cd_codigo, cd_filme, dt_data_inicio, dt_data_final, dt_data_efetiva, vl_custo_aluguel, cd_pedido, cd_pessoa, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
1	1	2015-02-01	2015-02-03	2015-02-03	12.30	\N	1	2015-12-07	2015-12-07	\N
2	2	2015-05-24	2015-05-27	2015-05-27	15.30	\N	1	2015-12-07	2015-12-07	\N
3	1	2014-12-10	2014-12-15	2014-12-15	13.50	\N	1	2015-12-07	2015-12-07	\N
4	2	2013-12-16	2013-12-17	2013-12-17	5.68	\N	1	2015-12-07	2015-12-07	\N
5	2	2015-05-24	2015-05-27	2015-05-27	15.30	\N	1	2015-12-07	2015-12-07	\N
6	1	2015-06-24	2015-06-26	2015-06-26	10.10	\N	1	2015-12-07	2015-12-07	\N
7	1	2015-02-01	2015-02-05	2015-02-05	18.90	\N	1	2015-12-07	2015-12-07	\N
8	2	2015-05-15	2015-05-16	2015-05-16	3.40	\N	1	2015-12-07	2015-12-07	\N
9	1	2015-05-01	2015-05-04	2015-05-04	12.80	\N	1	2015-12-07	2015-12-07	\N
10	2	2015-05-10	2015-05-12	2015-05-12	5.20	\N	1	2015-12-07	2015-12-07	\N
11	1	2015-05-08	2015-05-10	2015-05-10	6.40	\N	1	2015-12-07	2015-12-07	\N
\.


--
-- TOC entry 2439 (class 0 OID 0)
-- Dependencies: 173
-- Name: tb_aluguel_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_aluguel_cd_codigo_seq', 11, true);


--
-- TOC entry 2246 (class 0 OID 18045)
-- Dependencies: 174
-- Data for Name: tb_bairro; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_bairro (cd_codigo, tx_descricao, cd_municipio, nr_cep, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
\.


--
-- TOC entry 2440 (class 0 OID 0)
-- Dependencies: 175
-- Name: tb_bairro_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_bairro_cd_codigo_seq', 1, false);


--
-- TOC entry 2248 (class 0 OID 18050)
-- Dependencies: 176
-- Data for Name: tb_compra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_compra (cd_codigo, cd_fornecedor, vl_valor, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
\.


--
-- TOC entry 2441 (class 0 OID 0)
-- Dependencies: 177
-- Name: tb_compra_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_compra_cd_codigo_seq', 1, false);


--
-- TOC entry 2250 (class 0 OID 18055)
-- Dependencies: 178
-- Data for Name: tb_compraitem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_compraitem (cd_codigo, cd_midia, vl_preco, nr_quantidade, cd_compra, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
\.


--
-- TOC entry 2442 (class 0 OID 0)
-- Dependencies: 179
-- Name: tb_compraitem_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_compraitem_cd_codigo_seq', 1, false);


--
-- TOC entry 2252 (class 0 OID 18060)
-- Dependencies: 180
-- Data for Name: tb_elenco; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_elenco (cd_codigo, cd_filme, tx_nome, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
\.


--
-- TOC entry 2443 (class 0 OID 0)
-- Dependencies: 181
-- Name: tb_elenco_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_elenco_cd_codigo_seq', 1, false);


--
-- TOC entry 2254 (class 0 OID 18065)
-- Dependencies: 182
-- Data for Name: tb_estado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_estado (cd_codigo, tx_descricao, tx_sigla, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
\.


--
-- TOC entry 2444 (class 0 OID 0)
-- Dependencies: 183
-- Name: tb_estado_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_estado_cd_codigo_seq', 1, false);


--
-- TOC entry 2256 (class 0 OID 18070)
-- Dependencies: 184
-- Data for Name: tb_estoque; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_estoque (cd_codigo, cd_filme, nr_quantidade, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
\.


--
-- TOC entry 2445 (class 0 OID 0)
-- Dependencies: 185
-- Name: tb_estoque_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_estoque_cd_codigo_seq', 1, false);


--
-- TOC entry 2258 (class 0 OID 18075)
-- Dependencies: 186
-- Data for Name: tb_filme; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_filme (cd_codigo, cd_midia, tx_nome, cd_genero, nr_classificacao, vl_preco_diaria, dt_lancamento, nr_duracao, tx_idioma, tx_legenda, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
1	1	TARZAN	\N	13	\N	2013-04-01	60	\N	\N	2015-12-07	2015-12-07	\N
2	2	PIRATAS DO CARIBE	\N	16	\N	2014-06-12	180	\N	\N	2015-12-07	2015-12-07	\N
\.


--
-- TOC entry 2446 (class 0 OID 0)
-- Dependencies: 187
-- Name: tb_filme_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_filme_cd_codigo_seq', 2, true);


--
-- TOC entry 2260 (class 0 OID 18080)
-- Dependencies: 188
-- Data for Name: tb_fornecedor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_fornecedor (cd_codigo, tx_cnpj, tx_endereco, cd_municipio, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
\.


--
-- TOC entry 2447 (class 0 OID 0)
-- Dependencies: 189
-- Name: tb_fornecedor_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_fornecedor_cd_codigo_seq', 1, false);


--
-- TOC entry 2262 (class 0 OID 18085)
-- Dependencies: 190
-- Data for Name: tb_genero; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_genero (cd_codigo, tx_descricao, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
1	Ação	2015-12-07	2015-12-07	\N
2	Comédia	2015-12-07	2015-12-07	\N
3	Suspense	2015-12-07	2015-12-07	\N
4	Terror	2015-12-07	2015-12-07	\N
\.


--
-- TOC entry 2448 (class 0 OID 0)
-- Dependencies: 191
-- Name: tb_genero_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_genero_cd_codigo_seq', 4, true);


--
-- TOC entry 2264 (class 0 OID 18090)
-- Dependencies: 192
-- Data for Name: tb_imagem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_imagem (cd_codigo, cd_filme, tx_descricao, bt_foto, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
\.


--
-- TOC entry 2449 (class 0 OID 0)
-- Dependencies: 193
-- Name: tb_imagem_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_imagem_cd_codigo_seq', 1, false);


--
-- TOC entry 2266 (class 0 OID 18098)
-- Dependencies: 194
-- Data for Name: tb_midia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_midia (cd_codigo, tx_tipomidia, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
1	DVD	2015-12-07	2015-12-07	\N
2	BLURAY	2015-12-07	2015-12-07	\N
\.


--
-- TOC entry 2450 (class 0 OID 0)
-- Dependencies: 195
-- Name: tb_midia_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_midia_cd_codigo_seq', 2, true);


--
-- TOC entry 2268 (class 0 OID 18103)
-- Dependencies: 196
-- Data for Name: tb_municipio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_municipio (cd_codigo, tx_descricao, cd_estado, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
1	Descanso	\N	2015-12-07	2015-12-07	\N
2	Bandeirante	\N	2015-12-07	2015-12-07	\N
3	Paraíso	\N	2015-12-07	2015-12-07	\N
\.


--
-- TOC entry 2451 (class 0 OID 0)
-- Dependencies: 197
-- Name: tb_municipio_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_municipio_cd_codigo_seq', 3, true);


--
-- TOC entry 2270 (class 0 OID 18108)
-- Dependencies: 198
-- Data for Name: tb_pedido; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_pedido (cd_codigo, cd_filme, dt_data_pedido, dt_data_disponibilidade, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
\.


--
-- TOC entry 2452 (class 0 OID 0)
-- Dependencies: 199
-- Name: tb_pedido_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_pedido_cd_codigo_seq', 1, false);


--
-- TOC entry 2272 (class 0 OID 18113)
-- Dependencies: 200
-- Data for Name: tb_pessoa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_pessoa (cd_codigo, tx_nome, tx_cpf, tx_sexo, tx_endereco, cd_municipio, bt_foto, cd_tipo, dt_nascimento, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
1	Matheus	00000000191	\N	\N	\N	\N	1	\N	2015-12-07	2015-12-07	\N
\.


--
-- TOC entry 2453 (class 0 OID 0)
-- Dependencies: 201
-- Name: tb_pessoa_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_pessoa_cd_codigo_seq', 1, true);


--
-- TOC entry 2274 (class 0 OID 18121)
-- Dependencies: 202
-- Data for Name: tb_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_tipo (cd_codigo, tx_descricao, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
1	Cliente	2015-12-07	2015-12-07	\N
2	Funcionario	2015-12-07	2015-12-07	\N
3	Fornecedor	2015-12-07	2015-12-07	\N
\.


--
-- TOC entry 2454 (class 0 OID 0)
-- Dependencies: 203
-- Name: tb_tipo_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_tipo_cd_codigo_seq', 3, true);


--
-- TOC entry 2276 (class 0 OID 18126)
-- Dependencies: 204
-- Data for Name: tb_trailer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tb_trailer (cd_codigo, cd_filme, tx_descricao, bt_trailer, dt_cadastro, dt_manutencao, dt_exclusao) FROM stdin;
\.


--
-- TOC entry 2455 (class 0 OID 0)
-- Dependencies: 205
-- Name: tb_trailer_cd_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_trailer_cd_codigo_seq', 1, false);


--
-- TOC entry 2027 (class 2606 OID 18181)
-- Name: tb_aluguel_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_aluguel
    ADD CONSTRAINT tb_aluguel_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2030 (class 2606 OID 18183)
-- Name: tb_bairro_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_bairro
    ADD CONSTRAINT tb_bairro_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2033 (class 2606 OID 18185)
-- Name: tb_compra_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_compra
    ADD CONSTRAINT tb_compra_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2037 (class 2606 OID 18187)
-- Name: tb_compraitem_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_compraitem
    ADD CONSTRAINT tb_compraitem_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2040 (class 2606 OID 18189)
-- Name: tb_elenco_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_elenco
    ADD CONSTRAINT tb_elenco_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2043 (class 2606 OID 18191)
-- Name: tb_estado_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_estado
    ADD CONSTRAINT tb_estado_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2046 (class 2606 OID 18193)
-- Name: tb_estoque_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_estoque
    ADD CONSTRAINT tb_estoque_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2049 (class 2606 OID 18195)
-- Name: tb_filme_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_filme
    ADD CONSTRAINT tb_filme_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2052 (class 2606 OID 18197)
-- Name: tb_fornecedor_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_fornecedor
    ADD CONSTRAINT tb_fornecedor_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2055 (class 2606 OID 18199)
-- Name: tb_genero_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_genero
    ADD CONSTRAINT tb_genero_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2058 (class 2606 OID 18201)
-- Name: tb_imagem_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_imagem
    ADD CONSTRAINT tb_imagem_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2061 (class 2606 OID 18203)
-- Name: tb_midia_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_midia
    ADD CONSTRAINT tb_midia_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2064 (class 2606 OID 18205)
-- Name: tb_municipio_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_municipio
    ADD CONSTRAINT tb_municipio_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2067 (class 2606 OID 18207)
-- Name: tb_pedido_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_pedido
    ADD CONSTRAINT tb_pedido_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2070 (class 2606 OID 18209)
-- Name: tb_pessoa_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_pessoa
    ADD CONSTRAINT tb_pessoa_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2072 (class 2606 OID 18211)
-- Name: tb_tipo_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_tipo
    ADD CONSTRAINT tb_tipo_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2075 (class 2606 OID 18213)
-- Name: tb_trailer_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_trailer
    ADD CONSTRAINT tb_trailer_pk PRIMARY KEY (cd_codigo);


--
-- TOC entry 2025 (class 1259 OID 18214)
-- Name: aluguel_cd_pedido_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX aluguel_cd_pedido_sk ON tb_aluguel USING btree (cd_pedido);


--
-- TOC entry 2028 (class 1259 OID 18215)
-- Name: bairro_nr_cep_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX bairro_nr_cep_sk ON tb_bairro USING btree (nr_cep);


--
-- TOC entry 2031 (class 1259 OID 18216)
-- Name: compra_cd_fornecedor_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX compra_cd_fornecedor_sk ON tb_compra USING btree (cd_fornecedor);


--
-- TOC entry 2034 (class 1259 OID 18217)
-- Name: compraitem_cd_compra_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX compraitem_cd_compra_sk ON tb_compraitem USING btree (cd_compra);


--
-- TOC entry 2035 (class 1259 OID 18218)
-- Name: compraitem_cd_midia_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX compraitem_cd_midia_sk ON tb_compraitem USING btree (cd_midia);


--
-- TOC entry 2038 (class 1259 OID 18219)
-- Name: elenco_cd_filme_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX elenco_cd_filme_sk ON tb_elenco USING btree (cd_filme);


--
-- TOC entry 2041 (class 1259 OID 18220)
-- Name: estado_nr_sigla_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX estado_nr_sigla_sk ON tb_estado USING btree (tx_sigla);


--
-- TOC entry 2044 (class 1259 OID 18221)
-- Name: estoque_cd_filme_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX estoque_cd_filme_sk ON tb_estoque USING btree (cd_filme);


--
-- TOC entry 2047 (class 1259 OID 18222)
-- Name: filme_cd_midia_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX filme_cd_midia_sk ON tb_filme USING btree (cd_midia);


--
-- TOC entry 2050 (class 1259 OID 18223)
-- Name: fornecedor_cd_municipio_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX fornecedor_cd_municipio_sk ON tb_fornecedor USING btree (cd_municipio);


--
-- TOC entry 2053 (class 1259 OID 18224)
-- Name: genero_tx_descricao_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX genero_tx_descricao_sk ON tb_genero USING btree (tx_descricao);


--
-- TOC entry 2056 (class 1259 OID 18225)
-- Name: imagem_cd_filme_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX imagem_cd_filme_sk ON tb_imagem USING btree (cd_filme);


--
-- TOC entry 2059 (class 1259 OID 18226)
-- Name: midia_tx_tipomidia_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX midia_tx_tipomidia_sk ON tb_midia USING btree (tx_tipomidia);


--
-- TOC entry 2062 (class 1259 OID 18227)
-- Name: municipio_cd_estado_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX municipio_cd_estado_sk ON tb_municipio USING btree (cd_estado);


--
-- TOC entry 2065 (class 1259 OID 18228)
-- Name: pedido_cd_filme_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX pedido_cd_filme_sk ON tb_pedido USING btree (cd_filme);


--
-- TOC entry 2068 (class 1259 OID 18229)
-- Name: pessoa_tx_cpf_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX pessoa_tx_cpf_sk ON tb_pessoa USING btree (tx_cpf);


--
-- TOC entry 2073 (class 1259 OID 18230)
-- Name: tipo_tx_descricao_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX tipo_tx_descricao_sk ON tb_tipo USING btree (tx_descricao);


--
-- TOC entry 2076 (class 1259 OID 18231)
-- Name: trailer_cd_filme_sk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX trailer_cd_filme_sk ON tb_trailer USING btree (cd_filme);


--
-- TOC entry 2095 (class 2620 OID 18232)
-- Name: trg_befins_tb_aluguel; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_aluguel BEFORE INSERT ON tb_aluguel FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2097 (class 2620 OID 18233)
-- Name: trg_befins_tb_bairro; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_bairro BEFORE INSERT ON tb_bairro FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2099 (class 2620 OID 18234)
-- Name: trg_befins_tb_compra; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_compra BEFORE INSERT ON tb_compra FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2101 (class 2620 OID 18235)
-- Name: trg_befins_tb_compraitem; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_compraitem BEFORE INSERT ON tb_compraitem FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2103 (class 2620 OID 18236)
-- Name: trg_befins_tb_elenco; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_elenco BEFORE INSERT ON tb_elenco FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2105 (class 2620 OID 18237)
-- Name: trg_befins_tb_estado; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_estado BEFORE INSERT ON tb_estado FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2107 (class 2620 OID 18238)
-- Name: trg_befins_tb_estoque; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_estoque BEFORE INSERT ON tb_estoque FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2109 (class 2620 OID 18239)
-- Name: trg_befins_tb_filme; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_filme BEFORE INSERT ON tb_filme FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2111 (class 2620 OID 18240)
-- Name: trg_befins_tb_fornecedor; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_fornecedor BEFORE INSERT ON tb_fornecedor FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2113 (class 2620 OID 18241)
-- Name: trg_befins_tb_genero; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_genero BEFORE INSERT ON tb_genero FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2115 (class 2620 OID 18242)
-- Name: trg_befins_tb_imagem; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_imagem BEFORE INSERT ON tb_imagem FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2117 (class 2620 OID 18243)
-- Name: trg_befins_tb_midia; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_midia BEFORE INSERT ON tb_midia FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2119 (class 2620 OID 18244)
-- Name: trg_befins_tb_municipio; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_municipio BEFORE INSERT ON tb_municipio FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2121 (class 2620 OID 18245)
-- Name: trg_befins_tb_pedido; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_pedido BEFORE INSERT ON tb_pedido FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2123 (class 2620 OID 18246)
-- Name: trg_befins_tb_pessoa; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_pessoa BEFORE INSERT ON tb_pessoa FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2125 (class 2620 OID 18247)
-- Name: trg_befins_tb_tipo; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_tipo BEFORE INSERT ON tb_tipo FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2127 (class 2620 OID 18248)
-- Name: trg_befins_tb_trailer; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befins_tb_trailer BEFORE INSERT ON tb_trailer FOR EACH ROW EXECUTE PROCEDURE funct_insert_data();


--
-- TOC entry 2096 (class 2620 OID 18249)
-- Name: trg_befupd_tb_aluguel; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_aluguel BEFORE UPDATE ON tb_aluguel FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2098 (class 2620 OID 18250)
-- Name: trg_befupd_tb_bairro; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_bairro BEFORE UPDATE ON tb_bairro FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2100 (class 2620 OID 18251)
-- Name: trg_befupd_tb_compra; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_compra BEFORE UPDATE ON tb_compra FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2102 (class 2620 OID 18252)
-- Name: trg_befupd_tb_compraitem; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_compraitem BEFORE UPDATE ON tb_compraitem FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2104 (class 2620 OID 18253)
-- Name: trg_befupd_tb_elenco; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_elenco BEFORE UPDATE ON tb_elenco FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2106 (class 2620 OID 18254)
-- Name: trg_befupd_tb_estado; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_estado BEFORE UPDATE ON tb_estado FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2108 (class 2620 OID 18255)
-- Name: trg_befupd_tb_estoque; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_estoque BEFORE UPDATE ON tb_estoque FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2110 (class 2620 OID 18256)
-- Name: trg_befupd_tb_filme; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_filme BEFORE UPDATE ON tb_filme FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2112 (class 2620 OID 18257)
-- Name: trg_befupd_tb_fornecedor; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_fornecedor BEFORE UPDATE ON tb_fornecedor FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2114 (class 2620 OID 18258)
-- Name: trg_befupd_tb_genero; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_genero BEFORE UPDATE ON tb_genero FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2116 (class 2620 OID 18259)
-- Name: trg_befupd_tb_imagem; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_imagem BEFORE UPDATE ON tb_imagem FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2118 (class 2620 OID 18260)
-- Name: trg_befupd_tb_midia; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_midia BEFORE UPDATE ON tb_midia FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2120 (class 2620 OID 18261)
-- Name: trg_befupd_tb_municipio; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_municipio BEFORE UPDATE ON tb_municipio FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2122 (class 2620 OID 18262)
-- Name: trg_befupd_tb_pedido; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_pedido BEFORE UPDATE ON tb_pedido FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2124 (class 2620 OID 18263)
-- Name: trg_befupd_tb_pessoa; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_pessoa BEFORE UPDATE ON tb_pessoa FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2126 (class 2620 OID 18264)
-- Name: trg_befupd_tb_tipo; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_tipo BEFORE UPDATE ON tb_tipo FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2128 (class 2620 OID 18265)
-- Name: trg_befupd_tb_trailer; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_befupd_tb_trailer BEFORE UPDATE ON tb_trailer FOR EACH ROW EXECUTE PROCEDURE funct_update_data();


--
-- TOC entry 2077 (class 2606 OID 18266)
-- Name: tb_aluguel_cd_filme_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_aluguel
    ADD CONSTRAINT tb_aluguel_cd_filme_fkey FOREIGN KEY (cd_filme) REFERENCES tb_filme(cd_codigo);


--
-- TOC entry 2078 (class 2606 OID 18271)
-- Name: tb_aluguel_cd_pedido_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_aluguel
    ADD CONSTRAINT tb_aluguel_cd_pedido_fkey FOREIGN KEY (cd_pedido) REFERENCES tb_pedido(cd_codigo);


--
-- TOC entry 2079 (class 2606 OID 18276)
-- Name: tb_aluguel_cd_pessoa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_aluguel
    ADD CONSTRAINT tb_aluguel_cd_pessoa_fkey FOREIGN KEY (cd_pessoa) REFERENCES tb_pessoa(cd_codigo);


--
-- TOC entry 2080 (class 2606 OID 18281)
-- Name: tb_bairro_cd_municipio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_bairro
    ADD CONSTRAINT tb_bairro_cd_municipio_fkey FOREIGN KEY (cd_municipio) REFERENCES tb_municipio(cd_codigo);


--
-- TOC entry 2081 (class 2606 OID 18286)
-- Name: tb_compra_cd_fornecedor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_compra
    ADD CONSTRAINT tb_compra_cd_fornecedor_fkey FOREIGN KEY (cd_fornecedor) REFERENCES tb_fornecedor(cd_codigo);


--
-- TOC entry 2082 (class 2606 OID 18291)
-- Name: tb_compraitem_cd_compra_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_compraitem
    ADD CONSTRAINT tb_compraitem_cd_compra_fkey FOREIGN KEY (cd_compra) REFERENCES tb_compra(cd_codigo);


--
-- TOC entry 2083 (class 2606 OID 18296)
-- Name: tb_compraitem_cd_midia_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_compraitem
    ADD CONSTRAINT tb_compraitem_cd_midia_fkey FOREIGN KEY (cd_midia) REFERENCES tb_midia(cd_codigo);


--
-- TOC entry 2084 (class 2606 OID 18301)
-- Name: tb_elenco_cd_filme_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_elenco
    ADD CONSTRAINT tb_elenco_cd_filme_fkey FOREIGN KEY (cd_filme) REFERENCES tb_filme(cd_codigo);


--
-- TOC entry 2085 (class 2606 OID 18306)
-- Name: tb_estoque_cd_filme_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_estoque
    ADD CONSTRAINT tb_estoque_cd_filme_fkey FOREIGN KEY (cd_filme) REFERENCES tb_filme(cd_codigo);


--
-- TOC entry 2086 (class 2606 OID 18311)
-- Name: tb_filme_cd_genero_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_filme
    ADD CONSTRAINT tb_filme_cd_genero_fkey FOREIGN KEY (cd_genero) REFERENCES tb_genero(cd_codigo);


--
-- TOC entry 2087 (class 2606 OID 18316)
-- Name: tb_filme_cd_midia_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_filme
    ADD CONSTRAINT tb_filme_cd_midia_fkey FOREIGN KEY (cd_midia) REFERENCES tb_midia(cd_codigo);


--
-- TOC entry 2088 (class 2606 OID 18321)
-- Name: tb_fornecedor_cd_municipio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_fornecedor
    ADD CONSTRAINT tb_fornecedor_cd_municipio_fkey FOREIGN KEY (cd_municipio) REFERENCES tb_municipio(cd_codigo);


--
-- TOC entry 2089 (class 2606 OID 18326)
-- Name: tb_imagem_cd_filme_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_imagem
    ADD CONSTRAINT tb_imagem_cd_filme_fkey FOREIGN KEY (cd_filme) REFERENCES tb_filme(cd_codigo);


--
-- TOC entry 2090 (class 2606 OID 18331)
-- Name: tb_municipio_cd_estado_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_municipio
    ADD CONSTRAINT tb_municipio_cd_estado_fkey FOREIGN KEY (cd_estado) REFERENCES tb_estado(cd_codigo);


--
-- TOC entry 2091 (class 2606 OID 18336)
-- Name: tb_pedido_cd_filme_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_pedido
    ADD CONSTRAINT tb_pedido_cd_filme_fkey FOREIGN KEY (cd_filme) REFERENCES tb_filme(cd_codigo);


--
-- TOC entry 2092 (class 2606 OID 18341)
-- Name: tb_pessoa_cd_municipio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_pessoa
    ADD CONSTRAINT tb_pessoa_cd_municipio_fkey FOREIGN KEY (cd_municipio) REFERENCES tb_municipio(cd_codigo);


--
-- TOC entry 2093 (class 2606 OID 18346)
-- Name: tb_pessoa_cd_tipo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_pessoa
    ADD CONSTRAINT tb_pessoa_cd_tipo_fkey FOREIGN KEY (cd_tipo) REFERENCES tb_tipo(cd_codigo);


--
-- TOC entry 2094 (class 2606 OID 18351)
-- Name: tb_trailer_cd_filme_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_trailer
    ADD CONSTRAINT tb_trailer_cd_filme_fkey FOREIGN KEY (cd_filme) REFERENCES tb_filme(cd_codigo);


--
-- TOC entry 2284 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-12-07 16:08:55

--
-- PostgreSQL database dump complete
--

