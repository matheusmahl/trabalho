package br.edu.edii.locadora.visual;

import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import br.edu.edii.locadora.db.Conexao;
import br.edu.edii.locadora.helper.HelperTela;
import br.edu.edii.locadora.util.RelatoriosUtil;

import com.j256.ormlite.jdbc.JdbcDatabaseConnection;

public class RelatorioFilmesAlugados extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Principal telaPrincipal = null;
	private JPanel contentPane;
	private String filmesAlugados = "resources/reports/filmes_alugados.jasper";
	/**
	 * Create the frame.
	 * @param principal 
	 */
	public RelatorioFilmesAlugados(Principal telaPrincipal) {
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosed(InternalFrameEvent arg0) {
				RelatorioFilmesAlugados.this.telaPrincipal.telaFilmesAlugados = null;
			}
		});
		setClosable(true);
		setTitle("Emiss\u00E3o de filmes alugados");
		setBounds(100, 100, 361, 215);
		HelperTela.ConfigurarTela(this);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		Panel panel = new Panel();
		panel.setBounds(0, 0, 345, 185);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JButton btnGerar = new JButton("Gerar");
		btnGerar.setBounds(262, 151, 73, 23);
		panel.add(btnGerar);
		btnGerar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gerarFilmesAlugados();
			}
		});
		
		setVisible(true);
		this.telaPrincipal = telaPrincipal;
		telaPrincipal.desktopPane.add(this);
	}
	
	

	

	public void gerarFilmesAlugados() {
		try {
			RelatoriosUtil.geraRelatorio(filmesAlugados, null, 
					((JdbcDatabaseConnection) Conexao.getConexao().getReadWriteConnection()).getInternalConnection());
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Falha ao gerar relatório! Erro: " + e.getMessage());
		}
	}
}
