package br.edu.edii.locadora.visual;

import java.awt.ComponentOrientation;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.ParseException;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.text.MaskFormatter;

import br.edu.edii.locadora.controlador.CtrCadastroPessoa;
import br.edu.edii.locadora.helper.HelperData;
import br.edu.edii.locadora.helper.HelperString;
import br.edu.edii.locadora.helper.HelperTela;
import br.edu.edii.locadora.model.Pessoa;

public class CadastroPessoa extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Principal telaPrincipal = null;
	private JPanel contentPane;
	private CtrCadastroPessoa ControladorCadastro = new CtrCadastroPessoa();
	private JTextField txtNome;
	private JTextField txtCPF;
	private JTextField txtCodigo;
	private JTextField txtEndereco;
	private JTextField txtMunicipio;
	private JComboBox<String> cmbSexo;
	private JComboBox<String> cmbTipo;
	private JFormattedTextField txtDataNascimento;
	private JButton btnNovo;

	private boolean bFechando = false;

	/**
	 * Create the frame.
	 */
	public CadastroPessoa(Principal telaPrincipal) {
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosed(InternalFrameEvent arg0) {
				CadastroPessoa.this.telaPrincipal.telaCadastroPessoa = null;
			}

			@Override
			public void internalFrameClosing(InternalFrameEvent arg0) {
				bFechando = true;
			}
		});
		setClosable(true);
		setResizable(false);
		setSize(369, 355);
		setTitle("Cadastro de pessoas");

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		txtNome = new JTextField();
		txtNome.setBounds(90, 101, 253, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);

		JLabel lblNome = new JLabel("Nome:");
		lblNome.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		lblNome.setBounds(39, 104, 46, 14);
		contentPane.add(lblNome);

		txtCPF = new JTextField();
		txtCPF.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if ((!bFechando) && (!ControladorCadastro.ValidarCPF((txtCPF.getText().trim())))) {
					JOptionPane.showMessageDialog(null, "CPF inv�lido.");
					txtCPF.requestFocus();
				}
			}
		});
		txtCPF.setBounds(90, 70, 144, 20);
		contentPane.add(txtCPF);
		txtCPF.setColumns(10);

		JLabel lblCpf = new JLabel("CPF:");
		lblCpf.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		lblCpf.setBounds(53, 73, 32, 14);
		contentPane.add(lblCpf);

		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String codigo = JOptionPane.showInputDialog("Informe o codigo:");
				if ((codigo == null) || (codigo.isEmpty()))
					codigo = "0";
				int iCodigo = Integer.parseInt(codigo);
				if (iCodigo == 0)
					btnNovo.doClick();
				else {
					Pessoa pess = ControladorCadastro.buscar(iCodigo);
					if (pess != null) {
						txtCodigo.setText("" + pess.getCodigo());
						txtNome.setText(pess.getNome());
						txtCPF.setText(pess.getCPF());
						txtEndereco.setText(pess.getEndereco());
						cmbSexo.setSelectedIndex(GetIndexCombo(pess.getSexo()));
						cmbTipo.setSelectedIndex(GetIndexCombo(String.valueOf(pess.getTipo())));
						txtDataNascimento.setText(HelperData.DataParaString(pess.getNascimento()));
						txtMunicipio.setText(ControladorCadastro.BuscarMunicipio(pess.getCodigoMunicipio()));

					} else
						JOptionPane.showMessageDialog(null, "Codigo n�o encontrado");
				}
			}
		});
		btnBuscar.setBounds(10, 4, 89, 23);
		contentPane.add(btnBuscar);

		JLabel lblCdigo = new JLabel("C\u00F3digo:");
		lblCdigo.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		lblCdigo.setBounds(36, 42, 46, 14);
		contentPane.add(lblCdigo);

		txtCodigo = new JTextField();
		txtCodigo.setText("0");
		txtCodigo.setEditable(false);
		txtCodigo.setBounds(90, 39, 63, 20);
		contentPane.add(txtCodigo);
		txtCodigo.setColumns(10);

		btnNovo = new JButton("Novo");
		btnNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				HelperTela.limparTela(contentPane);
				txtCodigo.setText("0");
			}
		});
		btnNovo.setBounds(163, 38, 89, 23);
		contentPane.add(btnNovo);

		Panel pnlBotoes = new Panel();
		pnlBotoes.setBounds(0, 286, 353, 40);
		contentPane.add(pnlBotoes);
		pnlBotoes.setLayout(null);

		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.setBounds(265, 8, 78, 23);
		btnSalvar.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		pnlBotoes.add(btnSalvar);

		txtEndereco = new JTextField();
		txtEndereco.setColumns(10);
		txtEndereco.setBounds(90, 191, 253, 20);
		contentPane.add(txtEndereco);

		JLabel lblEndereo = new JLabel("Endere\u00E7o:");
		lblEndereo.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		lblEndereo.setBounds(13, 191, 72, 14);
		contentPane.add(lblEndereo);

		txtMunicipio = new JTextField();
		txtMunicipio.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if (!txtMunicipio.getText().isEmpty()) {
					// txtMunicipio.setText(ControladorCadastro.BuscarMunicipio(txtMunicipio.getText()));
					// txtMunicipio.setText("1");
				}
			}
		});
		txtMunicipio.setColumns(10);
		txtMunicipio.setBounds(90, 219, 253, 20);
		contentPane.add(txtMunicipio);

		JLabel lblMunicpio = new JLabel("Munic\u00EDpio:");
		lblMunicpio.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		lblMunicpio.setBounds(10, 222, 72, 14);
		contentPane.add(lblMunicpio);

		cmbSexo = new JComboBox<String>();
		cmbSexo.setModel(new DefaultComboBoxModel<String>(new String[] { "M - Masculino", "F - Feminino", "I - Indefinido" }));
		cmbSexo.setBounds(90, 132, 144, 20);
		contentPane.add(cmbSexo);

		JLabel lblSexo = new JLabel("Sexo:");
		lblSexo.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		lblSexo.setBounds(39, 135, 46, 14);
		contentPane.add(lblSexo);

		JLabel lblTipo = new JLabel("Tipo:");
		lblTipo.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		lblTipo.setBounds(13, 250, 72, 14);
		contentPane.add(lblTipo);

		cmbTipo = new JComboBox<String>();
		cmbTipo.setBounds(90, 247, 144, 20);
		contentPane.add(cmbTipo);
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (ValidarCampos()) {
					Pessoa p = new Pessoa();
					p.setCodigo(HelperString.RetornarNumeros(txtCodigo.getText()));
					p.setCPF(txtCPF.getText().trim());
					p.setNome(txtNome.getText().trim());
					p.setCodigoMunicipio(1);
					p.setEndereco(txtEndereco.getText().trim());
					p.setSexo(GetComboItemString(cmbSexo));
					p.setTipo(GetComboItemInt(cmbTipo));
					p.setNascimento(HelperData.FormatarData(txtDataNascimento.getText()));
					ControladorCadastro.salvar(p);
					btnNovo.doClick();
				}

			}
		});

		HelperTela.ConfigurarTela(this);
		setVisible(true);
		this.telaPrincipal = telaPrincipal;
		telaPrincipal.desktopPane.add(this);

		ControladorCadastro.CarregarTipo(cmbTipo);

		JLabel lblNascimento = new JLabel("Nascimento:");
		lblNascimento.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		lblNascimento.setBounds(13, 166, 72, 14);
		contentPane.add(lblNascimento);

		try {
			txtDataNascimento = new JFormattedTextField(new MaskFormatter("##/##/####"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		txtDataNascimento.setBounds(90, 163, 144, 20);
		contentPane.add(txtDataNascimento);
	}

	protected int GetIndexCombo(String texto) {
		if ((texto == null) || (texto.isEmpty()))
			return 0;

		for (int i = 0; i < cmbSexo.getItemCount(); i++) {
			if (cmbSexo.getItemAt(i).charAt(0) == texto.charAt(0))
				return i;
		}
		return 0;
	}

	protected String GetComboItemString(JComboBox<String> cmbBox) {
		String item = cmbBox.getItemAt(cmbBox.getSelectedIndex());
		return String.valueOf(item.charAt(0));
	}

	protected int GetComboItemInt(JComboBox<String> cmbBox) {
		String item = cmbBox.getItemAt(cmbBox.getSelectedIndex());
		try {
			return Integer.parseInt(String.valueOf(item.charAt(0)));
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	protected boolean ValidarCampos() {
		if (txtNome.getText().isEmpty() || txtNome.getText().length() < 6) {
			JOptionPane.showMessageDialog(this, "Campo nome deve conter no m�nimo 6 caracteres!");
			return false;
		}

		if (txtCPF.getText().isEmpty())
			return false;
		else if (!ControladorCadastro.ValidarCPF((txtCPF.getText().trim()))) {
			JOptionPane.showMessageDialog(this, "CPF inv�lido.");
			txtCPF.requestFocus();
			return false;
		}

		return true;
	}
}
