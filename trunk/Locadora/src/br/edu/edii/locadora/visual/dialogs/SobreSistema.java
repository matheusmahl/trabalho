package br.edu.edii.locadora.visual.dialogs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.edu.edii.locadora.db.Conexao;
import br.edu.edii.locadora.helper.HelperTela;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;

import java.awt.ComponentOrientation;
import javax.swing.JEditorPane;
import javax.swing.JTextArea;
import java.awt.Font;

public class SobreSistema extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Create the dialog.
	 */
	public SobreSistema() {
		setTitle("Sobre");
		setResizable(false);
		setBounds(100, 100, 396, 219);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel lblMlocadoraSistema = new JLabel("MLocal - Sistema de gest\u00E3o para para locadoras multi-videos");
		lblMlocadoraSistema.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 13));
		lblMlocadoraSistema.setBounds(10, 11, 380, 20);
		contentPanel.add(lblMlocadoraSistema);

		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBounds(10, 52, 370, 94);
		contentPanel.add(textArea);

		textArea.append(" Banco de dados: " + Conexao.getConexao().getDatabaseType().getDatabaseName());
		textArea.append("\n Java Version: " + System.getProperty("java.version") + " - "
				+ System.getProperty("java.vendor"));
		textArea.append("\n Usu�rio: " + System.getProperty("user.name"));
		textArea.append("\n Sistema operacional: " + System.getProperty("os.name") + " "
				+ System.getProperty("sun.os.patch.level"));

		HelperTela.ConfigurarTela(this);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Sair");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}
}
