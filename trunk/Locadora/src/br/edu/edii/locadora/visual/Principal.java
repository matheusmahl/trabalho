package br.edu.edii.locadora.visual;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDesktopPane;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.border.EmptyBorder;

import br.edu.edii.locadora.db.Conexao;
import br.edu.edii.locadora.visual.dialogs.SobreSistema;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class Principal extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JMenuBar menuBar = new JMenuBar();

	/* MENUS */
	private JMenu jmCadastro = new JMenu("Cadastros");
	private JMenu jmRelatorio = new JMenu("Relat�rios");
	private JMenu jmAjuda = new JMenu("Ajuda");

	/* ITENS DO MENU DE CADASTRO */
	private JMenuItem miCadastroPessoa = new JMenuItem("Pessoa");
	private JMenuItem miCadastroFilme = new JMenuItem("Filme");

	/* ITENS DO MENU AJUDA */
	private JMenuItem miSobre = new JMenuItem("Sobre");

	/* ITENS DO MENU RELATORIO */
	private JMenuItem miFilmesAlugados = new JMenuItem("Filmes alugados");

	/* OBJETOS DO MENU CADASTRO */
	protected CadastroPessoa telaCadastroPessoa;
	protected CadastroFilme telaCadastroFilme;

	protected RelatorioFilmesAlugados telaFilmesAlugados;

	JDesktopPane desktopPane;

	/**
	 * Create the frame.
	 */
	public Principal() {
		setTitle("MLocal");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 695, 431);

		desktopPane = new JDesktopPane();
		desktopPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		desktopPane.setBounds(0, 0, 679, 360);
		desktopPane.setLayout(null);
		setContentPane(desktopPane);

		// Ajusta a barra de menu na janela principal
		setJMenuBar(menuBar);

		// Adiciona os menus na barra de menus
		menuBar.add(jmCadastro);
		menuBar.add(jmRelatorio);
		menuBar.add(jmAjuda);

		/* ADI��O DE ITENS NO MENU DE CADASTRO */
		jmCadastro.add(miCadastroPessoa);
		jmCadastro.add(miCadastroFilme);

		/* ADI��O DE ITENS NO MENU AJUDA */
		jmAjuda.add(miSobre);

		/* ADI��O DE ITENS NO MENU RELATORIO */
		jmRelatorio.add(miFilmesAlugados);

		/* EVENTOS DOS ITENS DO MENU */
		miCadastroPessoa.addActionListener(this);
		miCadastroFilme.addActionListener(this);
		miFilmesAlugados.addActionListener(this);
		miSobre.addActionListener(this);


	}

	@Override
	public void actionPerformed(ActionEvent evento) {
		// se o evento capturado for uma chamada vinda do item cliente do
		// menu...
		if (evento.getSource() == miCadastroPessoa) {
			// se n�o for null, a tela j� est� vis�vel, tendo apenas que ser
			// "levada pra frente"
			if (telaCadastroPessoa == null)
				// instancia a tela de cadastro de clientes
				telaCadastroPessoa = new CadastroPessoa(this);

			// independente da tela estar vis�vel ou n�o, a tela � movida para
			// frente
			desktopPane.moveToFront(telaCadastroPessoa);
		} 
		else if (evento.getSource() == miCadastroFilme) {
			if (telaCadastroFilme == null)
				telaCadastroFilme = new CadastroFilme(this);

			desktopPane.moveToFront(telaCadastroFilme);
		}
		else if (evento.getSource() == miSobre) {
			SobreSistema telaSobreSistema = new SobreSistema();
			telaSobreSistema.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			telaSobreSistema.setVisible(true);
		}
		else if (evento.getSource() == miFilmesAlugados) {
			if (telaFilmesAlugados == null)
				telaFilmesAlugados = new RelatorioFilmesAlugados(this);

			desktopPane.moveToFront(telaFilmesAlugados);
		}

	}
}
