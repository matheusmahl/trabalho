package br.edu.edii.locadora.visual;

import java.awt.ComponentOrientation;
import java.awt.Panel;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import br.edu.edii.locadora.controlador.CtrCadastroFilme;
import br.edu.edii.locadora.helper.HelperData;
import br.edu.edii.locadora.helper.HelperString;
import br.edu.edii.locadora.helper.HelperTela;
import br.edu.edii.locadora.model.Filme;
import br.edu.edii.locadora.model.Pessoa;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CadastroFilme extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Principal telaPrincipal = null;
	private CtrCadastroFilme ControladorCadastro = new CtrCadastroFilme();
	private JPanel contentPane;
	private JTextField txtCodigo;
	private JTextField txtNome;
	private JComboBox<String> cmbMidia;
	private JComboBox<String> cmbGenero;
	private JButton btnNovo;

	/**
	 * Create the frame.
	 * @param principal 
	 */
	public CadastroFilme(Principal telaPrincipal) {
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosed(InternalFrameEvent arg0) {
				CadastroFilme.this.telaPrincipal.telaCadastroFilme = null;
			}
		});
		
		setClosable(true);
		setResizable(false);
		setSize(363,234);
		setTitle("Cadastro de filmes");
		
		HelperTela.ConfigurarTela(this);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		btnNovo = new JButton("Novo");
		btnNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				HelperTela.limparTela(contentPane);
				txtCodigo.setText("0");
			}
		});
		btnNovo.setBounds(152, 44, 89, 23);
		contentPane.add(btnNovo);
		
		txtCodigo = new JTextField();
		txtCodigo.setText("0");
		txtCodigo.setEditable(false);
		txtCodigo.setColumns(10);
		txtCodigo.setBounds(79, 45, 63, 20);
		contentPane.add(txtCodigo);
		
		JLabel label = new JLabel("C\u00F3digo:");
		label.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		label.setLabelFor(txtCodigo);
		label.setBounds(26, 48, 46, 14);
		contentPane.add(label);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String codigo = JOptionPane.showInputDialog("Informe o codigo:");
				if ((codigo == null) || (codigo.isEmpty()))
					codigo = "0";
				int iCodigo = Integer.parseInt(codigo);
				if (iCodigo == 0)
					btnNovo.doClick();
				else {
					Filme filme = ControladorCadastro.buscar(iCodigo);
					if (filme != null) {
						txtCodigo.setText("" + filme.getCodigo());
						txtNome.setText(filme.getNome());
						cmbGenero.setSelectedIndex(GetIndexCombo(String.valueOf(filme.getCodigoGenero()), cmbGenero));
						cmbMidia.setSelectedIndex(GetIndexCombo(String.valueOf(filme.getCodigoMidia()), cmbMidia));

					} else
						JOptionPane.showMessageDialog(null, "Codigo n�o encontrado");
				}
			}
		});
		btnBuscar.setBounds(26, 11, 89, 23);
		contentPane.add(btnBuscar);
		
		JLabel label_1 = new JLabel("Nome:");
		label_1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		label_1.setBounds(8, 77, 64, 14);
		contentPane.add(label_1);
		
		txtNome = new JTextField();
		txtNome.setColumns(10);
		txtNome.setBounds(79, 74, 221, 20);
		contentPane.add(txtNome);
		
		Panel panel = new Panel();
		panel.setLayout(null);
		panel.setBounds(0, 165, 347, 40);
		contentPane.add(panel);
		
		JButton button = new JButton("Salvar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (ValidarCampos()) {
				Filme filme = new Filme();
				filme.setCodigo(HelperString.RetornarNumeros(txtCodigo.getText()));
				filme.setNome(txtNome.getText().trim());
				filme.setCodigoMidia(GetComboItemInt(cmbMidia));
				filme.setCodigoGenero(GetComboItemInt(cmbGenero));
				ControladorCadastro.salvar(filme);
				btnNovo.doClick();
				}
			}
		});
		button.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		button.setBounds(259, 8, 78, 23);
		panel.add(button);
		
		cmbMidia = new JComboBox<String>();
		cmbMidia.setBounds(79, 102, 144, 20);
		contentPane.add(cmbMidia);
		
		JLabel lblMdia = new JLabel("M\u00EDdia:");
		lblMdia.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		lblMdia.setBounds(0, 105, 72, 14);
		contentPane.add(lblMdia);
		
		cmbGenero = new JComboBox<String>();
		cmbGenero.setBounds(79, 133, 144, 20);
		contentPane.add(cmbGenero);
		
		JLabel lblGnero = new JLabel("G\u00EAnero:");
		lblGnero.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		lblGnero.setBounds(0, 136, 72, 14);
		contentPane.add(lblGnero);
		
		setVisible(true);
		this.telaPrincipal = telaPrincipal;
		telaPrincipal.desktopPane.add(this);
		
		ControladorCadastro.CarregarMidia(cmbMidia);
		ControladorCadastro.CarregarGenero(cmbGenero);
		
	}

	protected boolean ValidarCampos() {
		if (txtNome.getText().isEmpty() || txtNome.getText().length() < 3) {
			JOptionPane.showMessageDialog(this, "Campo nome deve conter no m�nimo 3 caracteres!");
			return false;
		}
		return true;
	}
	
	protected int GetComboItemInt(JComboBox<String> cmbBox) {
		//TODO COLOCAR EM UM HELPER
		String item = cmbBox.getItemAt(cmbBox.getSelectedIndex());
		try {
			return Integer.parseInt(String.valueOf(item.charAt(0)));
		} catch (NumberFormatException e) {
			return 0;
		}
	}
	
	protected int GetIndexCombo(String texto, JComboBox<String> cmbBox) {
		if ((texto == null) || (texto.isEmpty()))
			return 0;

		for (int i = 0; i < cmbBox.getItemCount(); i++) {
			if (cmbBox.getItemAt(i).charAt(0) == texto.charAt(0))
				return i;
		}
		return 0;
	}
}
