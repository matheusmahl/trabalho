/**
 * 
 */
package br.edu.edii.locadora.util;

import java.sql.Connection;
import java.util.HashMap;

import javax.swing.JOptionPane;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 * @author robersonjfa
 * 
 */
public class RelatoriosUtil {
	public static void geraRelatorio(String caminhoRelatorio, HashMap<String, Object> parametros, Connection conexao) {
		// declarando objeto de impressao jasper
		JasperPrint jasperPrint = null;
		try {
			// preenche o relatorio
			jasperPrint = JasperFillManager.fillReport(caminhoRelatorio, parametros, conexao);
			// exibe o relatorio
			JasperViewer jw = new JasperViewer(jasperPrint, false);
			jw.setVisible(true);
		} catch (JRException e) {
			JOptionPane.showMessageDialog(null, "Falha ao gerar relatório! Erro: " + e.getMessage());
		}

	}
}