/**
 * 
 */
package br.edu.edii.locadora.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author MatheusMahl
 * 
 */

/*
 * CD_CODIGO SERIAL NOT NULL, 
 * TX_TIPOMIDIA VARCHAR(10),
 */
@DatabaseTable(tableName = "tb_midia")
public class Midia {
	@DatabaseField(columnName = "cd_codigo", generatedIdSequence = "tb_midia_cd_codigo_seq")
	private int codigo;
	@DatabaseField(columnName = "tx_tipomidia")
	private String tipomidia;
	/**
	 * @return the codigo
	 */
	public int getCodigo() {
		return codigo;
	}
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return the tipomidia
	 */
	public String getTipomidia() {
		return tipomidia;
	}
	/**
	 * @param tipomidia the tipomidia to set
	 */
	public void setTipomidia(String tipomidia) {
		this.tipomidia = tipomidia;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return codigo + " - " + tipomidia;
	}
}
