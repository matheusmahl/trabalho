/**
 * 
 */
package br.edu.edii.locadora.model;

import java.util.Currency;
import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author MatheusMahl
 *
 */
/*
 * CD_CODIGO SERIAL NOT NULL,
	CD_MIDIA INTEGER NOT NULL,
	TX_NOME VARCHAR(40) NOT NULL,
	CD_GENERO INTEGER,
	NR_CLASSIFICACAO INTEGER,
	VL_PRECO_DIARIA NUMERIC(8,2),
	DT_LANCAMENTO DATE,
	NR_DURACAO INTEGER,
	TX_IDIOMA VARCHAR(40),
	TX_LEGENDA VARCHAR(40),	
 */
@DatabaseTable(tableName = "tb_filme")
public class Filme {
	@DatabaseField(columnName = "cd_codigo", generatedIdSequence = "tb_filme_cd_codigo_seq")
	private int codigo;
	@DatabaseField(columnName = "tx_nome")
	private String nome;
	@DatabaseField(columnName = "cd_midia")
	private int codigoMidia;
	@DatabaseField(columnName = "cd_genero")
	private int codigoGenero;
	@DatabaseField(columnName = "nr_classificacao")
	private int classificacao;
	@DatabaseField(columnName = "vl_preco_diaria")
	private Double precoDiaria;
	@DatabaseField(columnName = "dt_lancamento")
	private Date dataLancamento;
	@DatabaseField(columnName = "nr_duracao")
	private int duracao;
	@DatabaseField(columnName = "tx_idioma")
	private String idioma;
	@DatabaseField(columnName = "tx_legenda")
	private String legenda;
	/**
	 * @return the codigo
	 */
	public int getCodigo() {
		return codigo;
	}
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @return the codigoMidia
	 */
	public int getCodigoMidia() {
		return codigoMidia;
	}
	/**
	 * @param codigoMidia the codigoMidia to set
	 */
	public void setCodigoMidia(int codigoMidia) {
		this.codigoMidia = codigoMidia;
	}
	/**
	 * @return the codigoGenero
	 */
	public int getCodigoGenero() {
		return codigoGenero;
	}
	/**
	 * @param codigoGenero the codigoGenero to set
	 */
	public void setCodigoGenero(int codigoGenero) {
		this.codigoGenero = codigoGenero;
	}
	/**
	 * @return the classificacao
	 */
	public int getClassificacao() {
		return classificacao;
	}
	/**
	 * @param classificacao the classificacao to set
	 */
	public void setClassificacao(int classificacao) {
		this.classificacao = classificacao;
	}
	/**
	 * @return the precoDiaria
	 */
	public Double getPrecoDiaria() {
		return precoDiaria;
	}
	/**
	 * @param precoDiaria the precoDiaria to set
	 */
	public void setPrecoDiaria(Double precoDiaria) {
		this.precoDiaria = precoDiaria;
	}
	/**
	 * @return the dataLancamento
	 */
	public Date getDataLancamento() {
		return dataLancamento;
	}
	/**
	 * @param dataLancamento the dataLancamento to set
	 */
	public void setDataLancamento(Date dataLancamento) {
		this.dataLancamento = dataLancamento;
	}
	/**
	 * @return the duracao
	 */
	public int getDuracao() {
		return duracao;
	}
	/**
	 * @param duracao the duracao to set
	 */
	public void setDuracao(int duracao) {
		this.duracao = duracao;
	}
	/**
	 * @return the idioma
	 */
	public String getIdioma() {
		return idioma;
	}
	/**
	 * @param idioma the idioma to set
	 */
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	/**
	 * @return the legenda
	 */
	public String getLegenda() {
		return legenda;
	}
	/**
	 * @param legenda the legenda to set
	 */
	public void setLegenda(String legenda) {
		this.legenda = legenda;
	}
	

}
