/**
 * 
 */
package br.edu.edii.locadora.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author MatheusMahl
 * 
 */

/*
 * 
 * CD_CODIGO SERIAL NOT NULL, 
 * TX_DESCRICAO VARCHAR(40),
 */
@DatabaseTable(tableName = "tb_genero")
public class Genero {
	@DatabaseField(columnName = "cd_codigo", generatedIdSequence = "tb_genero_cd_codigo_seq")
	private int codigo;
	@DatabaseField(columnName = "tx_descricao")
	private String descricao;
	/**
	 * @return the codigo
	 */
	public int getCodigo() {
		return codigo;
	}
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return codigo + " - " + descricao;
	}

}
