/**
 * 
 */
package br.edu.edii.locadora.model;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author mathe
 * 
 */
@DatabaseTable(tableName = "tb_tipo")
public class Tipo implements Serializable {
	private static final long serialVersionUID = 1L;
	@DatabaseField(columnName = "cd_codigo", generatedIdSequence = "tb_tipo_cd_codigo_seq")
	private int codigo;
	@DatabaseField(columnName = "tx_descricao")
	private String descricao;

	/**
	 * @return the codigo
	 */
	public int getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return codigo + " - " + descricao;
	}

}
