/**
 * 
 */
package br.edu.edii.locadora.helper;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

/**
 * @author mathe
 * 
 */
public class HelperTela {

	public static void ConfigurarTela(JInternalFrame tela) {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		tela.setLocation(dim.width / 2 - tela.getWidth() / 2, 150);

		for (int iIndex01 = 0; iIndex01 < tela.getComponentCount(); iIndex01++) {
			Component componente = tela.getComponent(iIndex01);
			if ((componente instanceof JPanel) && (componente.getName().equals("pnlBotoes"))) {
				// componente.setBounds(0, tela.getContentPane().getHeight()-
				// 40, tela.getContentPane().getWidth(), 40);
				// TODO Ajustar painel na tela
			}
		}

	}

	public static void ConfigurarTela(JDialog tela) {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		tela.setLocation(dim.width / 2 - tela.getWidth() / 2, 200);
	}

	public static void limparTela(JPanel painel) {
		for (Component componente : painel.getComponents()) {
			if (componente instanceof JTextField)
				((JTextComponent) componente).setText("");

		}

	}
}
