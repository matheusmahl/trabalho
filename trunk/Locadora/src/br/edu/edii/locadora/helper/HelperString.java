/**
 * 
 */
package br.edu.edii.locadora.helper;

/**
 * @author mathe
 * 
 */
public class HelperString {

	public static int RetornarNumeros(String texto) {
		int numeros = Integer.parseInt(texto.replaceAll("[^0123456789]", "").trim());
		
		if (texto.isEmpty()) {
			return 0;
		} else {
			return numeros;
		}
	}

}
