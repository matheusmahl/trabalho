/**
 * 
 */
package br.edu.edii.locadora.helper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author matheus
 *
 */
public class HelperData {
	
    public static Date FormatarData(String data) {   
        if (data == null || data.equals(""))  
            return null;  
          
        Date date = null;  
        try {  
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
            date = (java.util.Date)formatter.parse(data);  
        } catch (ParseException e) {              
              
        }  
        return date;  
    } 
    
    public static String DataParaString(Date data){
		SimpleDateFormat aux = new SimpleDateFormat("MM/dd/yyyy");
		if (data != null)
			return  aux.format(data);
    	return null;
    }

}
