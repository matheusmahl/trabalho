/**
 * 
 */
package br.edu.edii.locadora.main;

import javax.swing.JDialog;

import br.edu.edii.locadora.visual.Principal;

/**
 * @author Matheus
 * 
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		

		try {
			LoginDialog dialog = new LoginDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
			
			if (dialog.getResultado()) {

				Principal frame = new Principal();
				frame.setExtendedState(6);
				frame.setVisible(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
