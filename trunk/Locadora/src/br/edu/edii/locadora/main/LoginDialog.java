package br.edu.edii.locadora.main;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import br.edu.edii.locadora.db.Conexao;

import javax.swing.JPasswordField;

public class LoginDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtUsuario;
	private boolean resultado = false;
	private JPasswordField txtSenha;

	/**
	 * Create the dialog.
	 */
	public LoginDialog() {
		setResizable(false);
		setTitle("Login");
		setBounds(100, 100, 376, 181);
		getContentPane().setLayout(new BorderLayout());
		setLocationRelativeTo(null);  
		this.setModal(true);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel label = new JLabel("Usu\u00E1rio:");
			label.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
			label.setBounds(10, 36, 57, 14);
			contentPanel.add(label);
		}
		{
			txtUsuario = new JTextField();
			txtUsuario.setColumns(10);
			txtUsuario.setBounds(77, 33, 276, 20);
			contentPanel.add(txtUsuario);
		}
		{
			JLabel label = new JLabel("Senha:");
			label.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
			label.setBounds(10, 67, 57, 14);
			contentPanel.add(label);
		}

		txtSenha = new JPasswordField();
		txtSenha.setBounds(77, 64, 276, 20);
		contentPanel.add(txtSenha);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton btnLogar = new JButton("Logar");
				btnLogar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						Conexao.setUsuarioSenha(txtUsuario.getText().trim(), new String(txtSenha.getPassword()));
						if (Conexao.getConexao() != null) {
							resultado = true;
							dispose();
						} else {
							JOptionPane.showMessageDialog(null, "Usuario invalido");
						}
					}
				});
				btnLogar.setActionCommand("OK");
				buttonPane.add(btnLogar);
				getRootPane().setDefaultButton(btnLogar);
			}
			{
				JButton btnSair = new JButton("Sair");
				btnSair.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						resultado = false;
						dispose();
					}
				});
				btnSair.setActionCommand("Cancel");
				buttonPane.add(btnSair);
			}
		}
	}

	public boolean getResultado() {
		return resultado;
	}
}
