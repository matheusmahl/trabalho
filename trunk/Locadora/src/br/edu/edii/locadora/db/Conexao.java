/**
 * 
 */
package br.edu.edii.locadora.db;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

/**
 * @author Matheus
 * 
 */
public class Conexao {
	private final static String DB_PROPERTIES = "resources/conf/db.properties";
	private static Properties dbProperties;
	private static ConnectionSource conexao;
	private static String usuario;
	private static String senha;

	static {
		dbProperties = new Properties();
		try {

			dbProperties.load(new FileReader(DB_PROPERTIES));

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void setUsuarioSenha(String usu, String sen) {
		usuario = usu;
		senha = sen;
	}

	private Conexao() {
	}

	public static ConnectionSource getConexao() {
		if (conexao == null) {
			try {
				conexao = new JdbcConnectionSource(
						dbProperties.getProperty("url"), usuario, senha);

				conexao.getReadWriteConnection();
			} catch (SQLException e) {
				conexao = null;
			}
		}

		return conexao;
	}
}
