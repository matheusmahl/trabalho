/**
 * 
 */
package br.edu.edii.locadora.controlador;

import java.sql.SQLException;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import br.edu.edii.locadora.db.Conexao;
import br.edu.edii.locadora.model.Municipio;
import br.edu.edii.locadora.model.Pessoa;
import br.edu.edii.locadora.model.Tipo;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;

/**
 * @author mathe
 * 
 */
public class CtrCadastroPessoa {
	Dao<Pessoa, Integer> daoPessoa;
	Dao<Municipio, Integer> daoMunicipio;
	Dao<Tipo, Integer> daoTipo;
	private static String sqlMunicipio = "select cd_codigo from tb_municipio where tx_descricao ilike ? order by tx_descricao limit 1";

	public CtrCadastroPessoa() {
		// TODO Auto-generated constructor stub
		try {
			daoPessoa = DaoManager.createDao(Conexao.getConexao(), Pessoa.class);
			daoMunicipio = DaoManager.createDao(Conexao.getConexao(), Municipio.class);
			daoTipo = DaoManager.createDao(Conexao.getConexao(), Tipo.class);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void salvar(Pessoa p) {
		try {
			CreateOrUpdateStatus status = daoPessoa.createOrUpdate(p);
			if (status.isCreated())
				JOptionPane.showMessageDialog(null, "Salvo");
			else if (status.isUpdated())
				JOptionPane.showMessageDialog(null, "Atualizado");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "N�o foi poss�vel salvar!");
		}
	}

	public boolean existe(int codigo) {
		try {
			return (daoPessoa.idExists(codigo));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public Pessoa buscar(int codigo) {
		try {
			if ((codigo != 0) && (existe(codigo)))
				return daoPessoa.queryForId(codigo);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public boolean ValidarCPF(String CPF) {
		try {
			return daoPessoa.queryRawValue("select validarCPF(?)", CPF) == 1;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public String BuscarMunicipio(int codigo) {
		try {
			if ((codigo != 0) && (daoMunicipio.idExists(codigo)))
				return daoMunicipio.queryForId(codigo).toString();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	
	public void CarregarTipo(JComboBox<String> cmbTipo){
		try {
			List<Tipo> listaTipo = daoTipo.queryForAll();
			for (Tipo tipo : listaTipo) {
				cmbTipo.addItem(tipo.toString());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
