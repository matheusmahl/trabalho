/**
 * 
 */
package br.edu.edii.locadora.controlador;

import java.sql.SQLException;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import br.edu.edii.locadora.db.Conexao;
import br.edu.edii.locadora.model.Filme;
import br.edu.edii.locadora.model.Genero;
import br.edu.edii.locadora.model.Midia;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;
import com.j256.ormlite.dao.DaoManager;

/**
 * @author MatheusMahl
 *
 */
public class CtrCadastroFilme {
	private Dao<Filme, Integer> daoFilme;
	private Dao<Midia, Integer> daoMidia;
	private Dao<Genero, Integer> daoGenero;

	public CtrCadastroFilme() {
		// TODO Auto-generated constructor stub
		try {
			daoFilme = DaoManager.createDao(Conexao.getConexao(), Filme.class);
			daoMidia = DaoManager.createDao(Conexao.getConexao(), Midia.class);
			daoGenero = DaoManager.createDao(Conexao.getConexao(), Genero.class);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void salvar(Filme p) {
		try {
			CreateOrUpdateStatus status = daoFilme.createOrUpdate(p);
			if (status.isCreated())
				JOptionPane.showMessageDialog(null, "Salvo");
			else if (status.isUpdated())
				JOptionPane.showMessageDialog(null, "Atualizado");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "N�o foi poss�vel salvar!");
			e.printStackTrace();
		}
	}

	public boolean existe(int codigo) {
		try {
			return (daoFilme.idExists(codigo));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public Filme buscar(int codigo) {
		try {
			if ((codigo != 0) && (existe(codigo)))
				return daoFilme.queryForId(codigo);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void CarregarMidia(JComboBox<String> cmbMidia){
		try {
			List<Midia> listaMidia = daoMidia.queryForAll();
			for (Midia tipo : listaMidia) {
				cmbMidia.addItem(tipo.toString());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void CarregarGenero(JComboBox<String> cmbGenero) {
		try {
			List<Genero> listaGenero = daoGenero.queryForAll();
			for (Genero tipo : listaGenero) {
				cmbGenero.addItem(tipo.toString());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
