﻿/* View do relatório filmes alugados*/
create view vw_Rel_Filmes_Alugados as 
  select F.cd_codigo, F.tx_nome, M.TX_tipomidia, A.VL_CUSTO_ALUGUEL, A.DT_DATA_INICIO, A.DT_DATA_FINAL
    from tb_filme F
   inner join tb_aluguel A on A.CD_FILME = F.CD_CODIGO
   inner join tb_midia M  on F.cd_midia  = M.CD_CODIGO;

/* View do relatório de aniversariantes do mês */
create view vw_Rel_Aniversariantes as
select *
  from tb_pessoa
 where Extract(Month From dt_nascimento) = Extract(Month From Now());

/* View 1
Relação de clientes (nome, CPF, quantidade de alugueis, valor total gasto) por mês, 
nos anos de 2014 e 2015, somente para quantidades de aluguéis acima de cinco e valores acima de 30 reais. 
Ordem de relatório de clientes por mais aluguéis para o com menos aluguéis.*/

create view vw_Rel_clientes as
	select P.tx_nome, P.TX_CPF, COUNT(A.CD_CODIGO) as Quantidade, sum(A.VL_CUSTO_ALUGUEL) as ValorTotal
	  from tb_pessoa P
	 inner join tb_tipo T on P.CD_TIPO = T.CD_CODIGO
	 inner join tb_aluguel A on A.CD_PESSOA = P.CD_CODIGO
     where T.TX_descricao = 'Cliente'
	   and Extract(Year From A.DT_DATA_INICIO) in (2014, 2015)
	 group by P.tx_nome,P.TX_CPF, Extract(Month From A.DT_DATA_INICIO) 
	having COUNT(A.CD_CODIGO) > 5
	   and sum(A.VL_CUSTO_ALUGUEL) > 30
	 order by COUNT(A.CD_CODIGO) desc;

/* View 2
Relação de Aluguéis realizados em meses ímpares de 2014, para clientes do sexo feminino, 
de filmes dos gêneros ação, suspense e comédia, cujo ano de lançamento seja inferior a 2014. 
Ordem pelo nome do cliente Ascendente e data de aluguel descendente.*/

create view vw_Rel_Alugueis as
	select A.CD_CODIGO, P.TX_NOME as Pessoa, F.TX_NOME as Filme, G.TX_DESCRICAO, A.DT_DATA_INICIO, A.DT_DATA_EFETIVA, A.VL_CUSTO_ALUGUEL
	  from tb_aluguel A
	 inner join tb_pessoa P on A.CD_pessoa = P.CD_CODIGO
	 inner join tb_tipo T on P.CD_TIPO = T.CD_CODIGO
	 inner join tb_filme F on A.CD_FILME = F.CD_CODIGO
	 inner join tb_genero G on F.CD_GENERO = G.CD_CODIGO
	 where T.TX_descricao = 'Cliente'
  	   and P.TX_SEXO = 'F'
  	   and cast(Extract(Month From A.DT_DATA_INICIO) as Integer) % 2 = 0
  	   and Extract(Year From F.DT_LANCAMENTO) < 2014
 	   and G.TX_DESCRICAO in ('Ação', 'Comédia', 'Suspense')
	 order by P.TX_NOME asc, A.DT_DATA_INICIO DESC;

/* View 3
Relação de filmes lançados entre os anos de 2014 e 2015, locados por clientes das cidades de 
bandeirante, descanso e paraíso, e cuja classificação seja para maior ou igual a 14 anos. 
Ordem por nome do filme Ascendente.*/

create view vw_Rel_Filmes as
	Select F.TX_NOME as Filme, P.TX_NOME as Pessoa, M.TX_DESCRICAO as Municipio, F.DT_LANCAMENTO as Lancamento, F.NR_CLASSIFICACAO
	  from tb_aluguel A
	 inner join tb_pessoa P on A.CD_pessoa = P.CD_CODIGO
	 inner join tb_filme F on A.CD_FILME = F.CD_CODIGO
	 inner join tb_tipo T on P.CD_TIPO = T.CD_CODIGO
	 inner join tb_municipio M on P.CD_MUNICIPIO = M.CD_CODIGO
	 where T.TX_descricao = 'Cliente'
 	   and M.TX_DESCRICAO in ('Bandeirante', 'Descanso', 'Paraíso')
 	   and F.NR_Classificacao >= 14
 	   and Extract(year From F.DT_LANCAMENTO) >= 2014
  	   and Extract(year From F.DT_LANCAMENTO) <= 2015
 	 order by F.TX_NOME ASC;

/* View 4
Relação de clientes (nome e CPF, sexo Masculino, com mais de cinco anos de cadastro, 
que já reservaram filmes com mais de 2 horas de duração). Ordem por nome Ascendente.*/
create view vw_Rel_Clientes_2 as
	select P.TX_NOME as Pessoa, P.TX_CPF, P.TX_SEXO, P.DT_CADASTRO, F.TX_NOME as Filme, F.NR_DURACAO
	  from tb_aluguel A
	 inner join tb_pessoa P on A.CD_PESSOA = P.CD_CODIGO
	 inner join tb_tipo T on P.CD_TIPO = T.CD_CODIGO
	 inner join tb_filme F on A.CD_FILME = F.CD_CODIGO
	 where T.TX_descricao = 'Cliente'
	   and F.NR_DURACAO > 120
	   and cast(Extract(year from CURRENT_TIMESTAMP) as integer) - cast(Extract(Year From P.DT_CADASTRO) as integer) > 5
	 order by P.TX_NOME ASC;