﻿ALTER ROLE postgres ENCRYPTED PASSWORD '123456';
create role gerente ENCRYPTED PASSWORD '12345' LOGIN CREATEROLE NOCREATEDB;
create role funcionario ENCRYPTED PASSWORD '123' LOGIN NOCREATEROLE NOCREATEDB;

GRANT ALL ON tb_bairro, tb_estado, tb_tipo, tb_municipio, tb_fornecedor, tb_pessoa, tb_empresa, 
tb_genero, tb_aluguel, tb_estoque, tb_filme, tb_pedido, tb_elenco, tb_trailer, tb_imagem, tb_midia, tb_compraitem, tb_compra 
TO funcionario WITH GRANT OPTION;

GRANT ALL ON tb_bairro, tb_estado, tb_tipo, tb_municipio, tb_fornecedor, tb_pessoa, tb_empresa, 
tb_genero, tb_aluguel, tb_estoque, tb_filme, tb_pedido, tb_elenco, tb_trailer, tb_imagem, tb_midia, tb_compraitem, tb_compra 
TO gerente WITH GRANT OPTION;
