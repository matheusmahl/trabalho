﻿alter table TB_BAIRRO add constraint TB_BAIRRO_PK primary key (CD_CODIGO);
alter table TB_ESTADO add constraint TB_ESTADO_PK primary key (CD_CODIGO);
alter table TB_TIPO add constraint TB_TIPO_PK primary key (CD_CODIGO);
alter table TB_MUNICIPIO add constraint TB_MUNICIPIO_PK primary key (CD_CODIGO);
alter table TB_FORNECEDOR add constraint TB_FORNECEDOR_PK primary key (CD_CODIGO);
alter table TB_PESSOA add constraint TB_PESSOA_PK primary key (CD_CODIGO);
/*alter table TB_EMPRESA add constraint TB_EMPRESA_PK primary key (CD_CODIGO);*/
alter table TB_GENERO add constraint TB_GENERO_PK primary key (CD_CODIGO);
alter table TB_ALUGUEL add constraint TB_ALUGUEL_PK primary key (CD_CODIGO);
alter table TB_ESTOQUE add constraint TB_ESTOQUE_PK primary key (CD_CODIGO);
alter table TB_FILME add constraint TB_FILME_PK primary key (CD_CODIGO);
alter table TB_ELENCO add constraint TB_ELENCO_PK primary key (CD_CODIGO);
alter table TB_TRAILER add constraint TB_TRAILER_PK primary key (CD_CODIGO);
alter table TB_IMAGEM add constraint TB_IMAGEM_PK primary key (CD_CODIGO);
alter table TB_MIDIA add constraint TB_MIDIA_PK primary key (CD_CODIGO);
alter table TB_COMPRAITEM add constraint TB_COMPRAITEM_PK primary key (CD_CODIGO);
alter table TB_COMPRA add constraint TB_COMPRA_PK primary key (CD_CODIGO);
alter table TB_PEDIDO add constraint TB_PEDIDO_PK primary key (CD_CODIGO);

ALTER TABLE TB_ALUGUEL ADD FOREIGN KEY(CD_FILME) REFERENCES TB_FILME(CD_CODIGO);
ALTER TABLE TB_ALUGUEL ADD FOREIGN KEY(CD_PEDIDO) REFERENCES TB_PEDIDO(CD_CODIGO);
ALTER TABLE TB_ALUGUEL ADD FOREIGN KEY(CD_PESSOA) REFERENCES TB_PESSOA(CD_CODIGO);
ALTER TABLE TB_BAIRRO ADD FOREIGN KEY(CD_MUNICIPIO) REFERENCES TB_MUNICIPIO(CD_CODIGO);
ALTER TABLE TB_COMPRA ADD FOREIGN KEY(CD_FORNECEDOR) REFERENCES TB_FORNECEDOR(CD_CODIGO);
ALTER TABLE TB_COMPRAITEM ADD  FOREIGN KEY(CD_COMPRA) REFERENCES TB_COMPRA(CD_CODIGO);
ALTER TABLE TB_COMPRAITEM ADD FOREIGN KEY(CD_MIDIA) REFERENCES TB_MIDIA(CD_CODIGO);
ALTER TABLE TB_ELENCO ADD FOREIGN KEY(CD_FILME) REFERENCES TB_FILME(CD_CODIGO);
/*ALTER TABLE TB_EMPRESA ADD FOREIGN KEY(CD_MUNICIPIO) REFERENCES TB_MUNICIPIO(CD_CODIGO);
ALTER TABLE TB_EMPRESA ADD FOREIGN KEY(CD_ESTOQUE) REFERENCES TB_ESTOQUE(CD_CODIGO);*/
ALTER TABLE TB_ESTOQUE ADD FOREIGN KEY(CD_FILME) REFERENCES TB_FILME(CD_CODIGO);
ALTER TABLE TB_FILME ADD  FOREIGN KEY(CD_GENERO) REFERENCES TB_GENERO(CD_CODIGO);
ALTER TABLE TB_FILME ADD  FOREIGN KEY(CD_MIDIA) REFERENCES TB_MIDIA(CD_CODIGO);
ALTER TABLE TB_FORNECEDOR ADD FOREIGN KEY(CD_MUNICIPIO) REFERENCES TB_MUNICIPIO(CD_CODIGO);
ALTER TABLE TB_IMAGEM ADD  FOREIGN KEY(CD_FILME) REFERENCES TB_FILME(CD_CODIGO);
ALTER TABLE TB_MUNICIPIO ADD FOREIGN KEY(CD_ESTADO) REFERENCES TB_ESTADO(CD_CODIGO);
ALTER TABLE TB_PEDIDO ADD  FOREIGN KEY(CD_FILME) REFERENCES TB_FILME(CD_CODIGO);
ALTER TABLE TB_PESSOA ADD  FOREIGN KEY(CD_MUNICIPIO) REFERENCES TB_MUNICIPIO(CD_CODIGO);
ALTER TABLE TB_PESSOA ADD FOREIGN KEY(CD_TIPO) REFERENCES TB_TIPO(CD_CODIGO);
ALTER TABLE TB_TRAILER ADD  FOREIGN KEY(CD_FILME) REFERENCES TB_FILME(CD_CODIGO);














    