﻿create trigger trg_befupd_TB_PESSOA before update on TB_PESSOA
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_PESSOA before insert on TB_PESSOA
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_BAIRRO before update on TB_BAIRRO
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_BAIRRO before insert on TB_BAIRRO
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_ESTADO before update on TB_ESTADO
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_ESTADO before insert on TB_ESTADO
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_TIPO before update on TB_TIPO
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_TIPO before insert on TB_TIPO
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_FORNECEDOR before update on TB_FORNECEDOR
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_FORNECEDOR before insert on TB_FORNECEDOR
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_MUNICIPIO before update on TB_MUNICIPIO
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_MUNICIPIO before insert on TB_MUNICIPIO
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_GENERO before update on TB_GENERO
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_GENERO before insert on TB_GENERO
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_ALUGUEL before update on TB_ALUGUEL
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_ALUGUEL before insert on TB_ALUGUEL
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_ESTOQUE before update on TB_ESTOQUE
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_ESTOQUE before insert on TB_ESTOQUE
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_FILME before update on TB_FILME
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_FILME before insert on TB_FILME
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_PEDIDO before update on TB_PEDIDO
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_PEDIDO before insert on TB_PEDIDO
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_ELENCO before update on TB_ELENCO
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_ELENCO before insert on TB_ELENCO
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_TRAILER before update on TB_TRAILER
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_TRAILER before insert on TB_TRAILER
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_IMAGEM before update on TB_IMAGEM
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_IMAGEM before insert on TB_IMAGEM
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_MIDIA before update on TB_MIDIA
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_MIDIA before insert on TB_MIDIA
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_COMPRAITEM before update on TB_COMPRAITEM
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_COMPRAITEM before insert on TB_COMPRAITEM
for each row execute procedure funct_insert_data();

create trigger trg_befupd_TB_COMPRA before update on TB_COMPRA
for each row execute procedure funct_update_data();

create trigger trg_befins_TB_COMPRA before insert on TB_COMPRA
for each row execute procedure funct_insert_data();