﻿comment on table TB_BAIRRO is 'Tabela de bairros';
comment on column TB_BAIRRO.CD_CODIGO is 'Código do bairro';
comment on column TB_BAIRRO.TX_DESCRICAO is 'Descrição do bairro';
comment on column TB_BAIRRO.NR_CEP is 'CEP do bairro';
comment on column TB_BAIRRO.DT_CADASTRO is 'Data de cadastro';
comment on column TB_BAIRRO.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_BAIRRO.DT_EXCLUSAO is 'Data de exclusão';

comment on table TB_ESTADO is 'Tabela de estados';
comment on column TB_ESTADO.CD_CODIGO is 'Código do estado';
comment on column TB_ESTADO.TX_DESCRICAO is 'Descrição do estado';
comment on column TB_ESTADO.TX_SIGLA is 'Sigla do estado';
comment on column TB_ESTADO.DT_CADASTRO is 'Data de cadastro';
comment on column TB_ESTADO.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_ESTADO.DT_EXCLUSAO is 'Data de exclusão';

comment on table TB_TIPO is 'Tabela de tipos';
comment on column TB_TIPO.CD_CODIGO is 'Código do tipo';
comment on column TB_TIPO.TX_DESCRICAO is 'Descrição do tipo';
comment on column TB_TIPO.DT_CADASTRO is 'Data de cadastro';
comment on column TB_TIPO.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_TIPO.DT_EXCLUSAO is 'Data de exclusão';

comment on table TB_MUNICIPIO is 'Tabela de municipios';
comment on column TB_MUNICIPIO.CD_CODIGO is 'Código do municipio';
comment on column TB_MUNICIPIO.TX_DESCRICAO is 'Descrição do municipio';
comment on column TB_MUNICIPIO.CD_ESTADO is 'Código do estado do municipio';
comment on column TB_MUNICIPIO.DT_CADASTRO is 'Data de cadastro';
comment on column TB_MUNICIPIO.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_MUNICIPIO.DT_EXCLUSAO is 'Data de exclusão';

comment on table TB_FORNECEDOR is 'Tabela de fornecedores';
comment on column TB_FORNECEDOR.CD_CODIGO is 'Código do fornecedor';
comment on column TB_FORNECEDOR.TX_CNPJ is 'CNPJ do fornecedor';
comment on column TB_FORNECEDOR.TX_ENDERECO is 'Endereço do fornecedor';
comment on column TB_FORNECEDOR.CD_MUNICIPIO is 'Código do municipio do fornecedor';
comment on column TB_FORNECEDOR.DT_CADASTRO is 'Data de cadastro';
comment on column TB_FORNECEDOR.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_FORNECEDOR.DT_EXCLUSAO is 'Data de exclusão';

comment on table TB_PESSOA is 'Tabela de pessoas';
comment on column TB_PESSOA.CD_CODIGO is 'Código da pessoa';
comment on column TB_PESSOA.TX_CPF is 'CPF da pessoa';
comment on column TB_PESSOA.TX_ENDERECO is 'Endereço da pessoa';
comment on column TB_PESSOA.CD_MUNICIPIO is 'Municipio da pessoa';
comment on column TB_PESSOA.BT_FOTO is 'Foto da pessoa';
comment on column TB_PESSOA.CD_TIPO is 'Tipo da pessoa';
comment on column TB_PESSOA.DT_CADASTRO is 'Data de cadastro';
comment on column TB_PESSOA.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_PESSOA.DT_EXCLUSAO is 'Data de exclusão';
/*
comment on table TB_EMPRESA is 'Tabela de empresas';
comment on column TB_EMPRESA.CD_CODIGO is 'Código da empresa';
comment on column TB_EMPRESA.CD_ESTOQUE is 'Estoque da empresa';
comment on column TB_EMPRESA.TX_RAZAO_SOCIAL is 'Razão social da empresa';
comment on column TB_EMPRESA.TX_CNPJ is 'CNPJ da empresa';
comment on column TB_EMPRESA.CD_MUNICIPIO is 'Código do municipio da empresa';
comment on column TB_EMPRESA.TX_ENDERECO is 'Endereço da empresa';
comment on column TB_EMPRESA.DT_CADASTRO is 'Data de cadastro';
comment on column TB_EMPRESA.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_EMPRESA.DT_EXCLUSAO is 'Data de exclusão';
*/
comment on table TB_GENERO is 'Tabela de generos';
comment on column TB_GENERO.CD_CODIGO is 'Código do genero';
comment on column TB_GENERO.TX_DESCRICAO is 'Descrição do genero';
comment on column TB_GENERO.DT_CADASTRO is 'Data de cadastro';
comment on column TB_GENERO.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_GENERO.DT_EXCLUSAO is 'Data de exclusão';

comment on table TB_ALUGUEL is 'Tabela de alugueis';
comment on column TB_ALUGUEL.CD_CODIGO is 'Código do aluguel';
comment on column TB_ALUGUEL.CD_FILME is 'Código do filme do aluguel';
comment on column TB_ALUGUEL.DT_DATA_INICIO is 'Data inicial do aluguel';
comment on column TB_ALUGUEL.DT_DATA_FINAL is 'Data final do aluguel';
comment on column TB_ALUGUEL.DT_DATA_EFETIVA is 'Data efetiva do aluguel';
comment on column TB_ALUGUEL.VL_CUSTO_ALUGUEL is 'Valor do aluguel';
comment on column TB_ALUGUEL.CD_PEDIDO is 'Código do pedido';
comment on column TB_ALUGUEL.CD_PESSOA is 'Código da pessoa';
comment on column TB_ALUGUEL.DT_CADASTRO is 'Data de cadastro';
comment on column TB_ALUGUEL.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_ALUGUEL.DT_EXCLUSAO is 'Data de exclusão';

comment on table TB_ESTOQUE is 'Tabela de estoques';
comment on column TB_ESTOQUE.CD_CODIGO is 'Código do estoque';
comment on column TB_ESTOQUE.CD_FILME is 'Código do filme no estoque';
comment on column TB_ESTOQUE.NR_QUANTIDADE is 'Quantidade do estoque';
comment on column TB_ESTOQUE.DT_CADASTRO is 'Data de cadastro';
comment on column TB_ESTOQUE.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_ESTOQUE.DT_EXCLUSAO is 'Data de exclusão';

comment on table TB_FILME is 'Tabela de filmes';
comment on column TB_FILME.CD_CODIGO is 'Código do filme';
comment on column TB_FILME.CD_MIDIA is 'Código da mídia do filme';
comment on column TB_FILME.TX_NOME is 'Nome do fime';
comment on column TB_FILME.CD_GENERO is 'Genero do filme';
comment on column TB_FILME.NR_CLASSIFICACAO is 'Classificação do filme';
comment on column TB_FILME.VL_PRECO_DIARIA is 'Valor da diária do filme';
comment on column TB_FILME.NR_DURACAO is 'Duração do filmeo';
comment on column TB_FILME.TX_IDIOMA is 'Idioma do filme';
comment on column TB_FILME.TX_LEGENDA is 'Legenda do filme';
comment on column TB_FILME.DT_LANCAMENTO is 'Data de lançamento do filme';
comment on column TB_FILME.DT_CADASTRO is 'Data de cadastro';
comment on column TB_FILME.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_FILME.DT_EXCLUSAO is 'Data de exclusão';

comment on table TB_PEDIDO is 'Tabela de pedidos';
comment on column TB_PEDIDO.CD_CODIGO is 'Código do pedido';
comment on column TB_PEDIDO.CD_FILME is 'Código do filme do pedido';
comment on column TB_PEDIDO.DT_DATA_PEDIDO is 'Data do pedido';
comment on column TB_PEDIDO.DT_DATA_PEDIDO is 'Data da disponibilidade dos filmes do pedido';
comment on column TB_PEDIDO.DT_CADASTRO is 'Data de cadastro';
comment on column TB_PEDIDO.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_PEDIDO.DT_EXCLUSAO is 'Data de exclusão';

comment on table TB_ELENCO is 'Tabela de elencos';
comment on column TB_ELENCO.CD_CODIGO is 'Código do elenco';
comment on column TB_ELENCO.CD_FILME is 'Código do filme do elenco';
comment on column TB_ELENCO.TX_NOME is 'Nomes do elenco';
comment on column TB_ELENCO.DT_CADASTRO is 'Data de cadastro';
comment on column TB_ELENCO.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_ELENCO.DT_EXCLUSAO is 'Data de exclusão';

comment on table TB_TRAILER is 'Tabela de trailers';
comment on column TB_TRAILER.CD_CODIGO is 'Código do trailer';
comment on column TB_TRAILER.CD_FILME is 'Código do filme do trailer';
comment on column TB_TRAILER.TX_DESCRICAO is 'Descrição do trailer';
comment on column TB_TRAILER.BT_TRAILER is 'Video do trailer ';
comment on column TB_TRAILER.DT_CADASTRO is 'Data de cadastro';
comment on column TB_TRAILER.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_TRAILER.DT_EXCLUSAO is 'Data de exclusão';

comment on table TB_IMAGEM is 'Tabela de imagens';
comment on column TB_IMAGEM.CD_CODIGO is 'Código da imagem';
comment on column TB_IMAGEM.CD_FILME is 'Código do filme da imagem';
comment on column TB_IMAGEM.TX_DESCRICAO is 'Descrição da imagem';
comment on column TB_IMAGEM.BT_FOTO is 'Imagem';
comment on column TB_IMAGEM.DT_CADASTRO is 'Data de cadastro';
comment on column TB_IMAGEM.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_IMAGEM.DT_EXCLUSAO is 'Data de exclusão';

comment on table TB_MIDIA is 'Tabela de mídias';
comment on column TB_MIDIA.CD_CODIGO is 'Código da mídia';
comment on column TB_MIDIA.TX_TIPOMIDIA is 'Tipo da mídia';
comment on column TB_MIDIA.DT_CADASTRO is 'Data de cadastro';
comment on column TB_MIDIA.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_MIDIA.DT_EXCLUSAO is 'Data de exclusão';

comment on table TB_COMPRAITEM is 'Tabela de compras';
comment on column TB_COMPRAITEM.CD_CODIGO is 'Código do item comprado';
comment on column TB_COMPRAITEM.CD_MIDIA is 'Tipo da mídia';
comment on column TB_COMPRAITEM.VL_PRECO is 'Valor do item';
comment on column TB_COMPRAITEM.NR_QUANTIDADE is 'Quantidade comprada';
comment on column TB_COMPRAITEM.CD_COMPRA is 'Código da compra';
comment on column TB_COMPRAITEM.DT_CADASTRO is 'Data de cadastro';
comment on column TB_COMPRAITEM.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_COMPRAITEM.DT_EXCLUSAO is 'Data de exclusão';

comment on table TB_COMPRA is 'Tabela de compras';
comment on column TB_COMPRA.CD_CODIGO is 'Código da comprado';
comment on column TB_COMPRA.CD_FORNECEDOR is 'Código do fornecedor';
comment on column TB_COMPRA.VL_VALOR is 'Valor da compra';
comment on column TB_COMPRA.DT_CADASTRO is 'Data de cadastro';
comment on column TB_COMPRA.DT_MANUTENCAO is 'Data de manutenção';
comment on column TB_COMPRA.DT_EXCLUSAO is 'Data de exclusão';
